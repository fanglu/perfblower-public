# PerfBlower #

## The Workflow of PerfBlower ##
1. Users write ISL program, and our ISL parser generate the Java code for the ISL programs.
2. The generated Java code is merged to the JVM.
3. The programs which we want to test are executed on the modified JVM.

## How to Use PerfBlower Parser ##
The driver class is org.mmtk.amplifier.SDLtoJAVA.
You can use the first parameter to specify the input ISL program.

## How to Merge the Generated Java Files into JVM ##
You have to manually merge the generated Java files into the original JVM.
Currently, we are developing the script and library to help you automatically merge the generated files into the JVM.
We will update the project soon.

## Developers ##
[Lu Fang](http://www.ics.uci.edu/~lfang3/) (main maintainer, currently interning at Google, will resume the development of the project in late September.)

Matthew Hartz

[Harry Xu](http://www.ics.uci.edu/~guoqingx/)