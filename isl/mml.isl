//General Memory Leak Amplifier

Context ObjContext {
	type = "java.lang.Object";
}

History UseHistory {
	type = "boolean";
	size = 10 ;
}

Partition P {
   kind = all;
   history= UseHistory;
}

TObject MyObj{
	include = ObjContext;
	partition = P;
	instance boolean useFlag ;
	instance boolean testFlag ;
}

TObject HerObj{
	include = ObjContext;
	partition = P;
	instance boolean useFlag ;
	instance boolean testFlag ;
}

TObject HisObj{
	include = ObjContext;
	partition = P;
	instance boolean useFlag ;
	instance boolean testFlag ;
}

Event on_rw(Object o, Field f,Word w1, Word w2){
	setMetaData(o, useFlag, true);
	deamplify(o);
}

Event on_reachedOnce(Object o){
	UseHistory h = (UseHistory) getHistory(o);
	h.update(getMetaData(o, useFlag));
	if(h.isFull() && !h.contains(true)) {
		amplify(o);
	}
}