//General Under Utilized Container

Context ObjContext {
	type = "java.lang.Object";
}

History UtilityRates {
	type = "double";
	size = 10; 
}

Partition P {
	kind = all;
	history = UtilityRates; 
}

TObject MyObj{
	include = ObjContext;
	partition = P; 
}


Event on_reachedOnce(Object o){
	if (!MiscHeader.isArray(o)){
		return;
	}
	UtilityRates h = getHistory(o);
	Object[] arr = (Object[]) o;
	int numNonNull = 0;
	for(Object ele : arr){ 
		if(ele == null) numNonNull++;
	}
	double rate = (double)numNonNull/arr.length;
	h.update(rate);
	if(rate > 0.5) {
		deamplify(o);
	}else if(h.isFull()){
		for(int i = 0; i < 10; i ++) {
			if(h.get(i) > 0.5) return;
		}
		amplify(o);
	} 
}