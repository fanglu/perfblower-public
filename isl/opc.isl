//General Under Utilized Container

Context ObjContext {
	type = "java.lang.Object";
}

History StaleRates {
	type = "double";
	size = 3; 
}

Partition P {
	kind = all;
	history = StaleRates; 
}

TObject ArrayObj{
	include = ObjContext;
	partition = P;
	instance byte staleness;
	instance byte test1;
	instance short test3;
}

Event on_rw(Object o, Field f,Word w1, Word w2){
	setMetaData(o, staleness, 0);
}

Event on_reachedOnce(Object o){
	StaleRates h = getHistory(o);

	setMetaData(o, staleness, getMetaData(o, staleness) + 1);
	if (!MiscHeader.isObjArray(o)){
		return;
	}
	Object[] arr = (Object[]) o;
	int numStale = 0;
	for(Object ele : arr){ 
		if(getMetaData(ele, staleness) >= 5) numStale++;
	}
	double rate = (double)numStale/arr.length;
	h.update(rate);
	if(rate < 0.5) {
		deamplify(o);
	}else if(h.isFull()){
		for(int i = 0; i < 10; i ++) {
			if(h.get(i) < 0.5) return;
		}
		amplify(o);
	} 
}