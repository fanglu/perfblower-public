package org.mmtk.amplifier;
import java.util.HashMap;
import java.util.Map;

public class ContextVisitor extends sdlParserBaseVisitor<String> {
	Map<String, String> contextDefs = new HashMap<String, String>();

	public String visitC(sdlParserParser.CContext ctx) {
		String ctxName = ctx.Identifier().getText();
		String ctxSTP = visit(ctx.cb());
		contextDefs.put(ctxName, ctxSTP);
		return ctxName;
	}

	public String visitSTP(sdlParserParser.STPContext ctx) {
		String s = null;
		String t = null;
		String p = null;
		String result = null;
		if (ctx.PATH() != null)
			p = ctx.Identifier().getText();
		if (ctx.SEQUENCE() != null) {
			if (ctx.TYPE() != null) {
				s = ctx.STR(0).getText();
				t = ctx.STR(1).getText();
			} else
				s = ctx.STR(0).getText();
		} else if (ctx.TYPE() != null) {
			t = ctx.STR(0).getText();
		}
		result = s + "|" + t + "|" + p;
		return result;
	}

	public String visitSPT(sdlParserParser.SPTContext ctx) {
		String s = null;
		String t = null;
		String p = null;
		String result = null;
		if (ctx.PATH() != null)
			p = ctx.Identifier().getText();
		if (ctx.SEQUENCE() != null) {
			if (ctx.TYPE() != null) {
				s = ctx.STR(0).getText();
				t = ctx.STR(1).getText();
			} else
				s = ctx.STR(0).getText();
		} else if (ctx.TYPE() != null) {
			t = ctx.STR(0).getText();
		}
		result = s + "|" + t + "|" + p;
		return result;
	}

	public String visitPST(sdlParserParser.PSTContext ctx) {
		String s = null;
		String t = null;
		String p = null;
		String result = null;
		if (ctx.PATH() != null)
			p = ctx.Identifier().getText();
		if (ctx.SEQUENCE() != null) {
			if (ctx.TYPE() != null) {
				s = ctx.STR(0).getText();
				t = ctx.STR(1).getText();
			} else
				s = ctx.STR(0).getText();
		} else if (ctx.TYPE() != null) {
			t = ctx.STR(0).getText();
		}
		result = s + "|" + t + "|" + p;
		return result;
	}

	public String visitPTS(sdlParserParser.PTSContext ctx) {
		String s = null;
		String t = null;
		String p = null;
		String result = null;
		if (ctx.PATH() != null)
			p = ctx.Identifier().getText();
		if (ctx.SEQUENCE() != null) {
			if (ctx.TYPE() != null) {
				s = ctx.STR(0).getText();
				t = ctx.STR(1).getText();
			} else
				s = ctx.STR(0).getText();
		} else if (ctx.TYPE() != null) {
			t = ctx.STR(0).getText();
		}
		result = s + "|" + t + "|" + p;
		return result;
	}

	public String visitTPS(sdlParserParser.TPSContext ctx) {
		String s = null;
		String t = null;
		String p = null;
		String result = null;
		if (ctx.PATH() != null)
			p = ctx.Identifier().getText();
		if (ctx.SEQUENCE() != null) {
			if (ctx.TYPE() != null) {
				s = ctx.STR(0).getText();
				t = ctx.STR(1).getText();
			} else
				s = ctx.STR(0).getText();
		} else if (ctx.TYPE() != null) {
			t = ctx.STR(0).getText();
		}
		result = s + "|" + t + "|" + p;
		return result;
	}

	public String visitTSP(sdlParserParser.TSPContext ctx) {
		String s = null;
		String t = null;
		String p = null;
		String result = null;
		if (ctx.PATH() != null)
			p = ctx.Identifier().getText();
		if (ctx.SEQUENCE() != null) {
			if (ctx.TYPE() != null) {
				s = ctx.STR(0).getText();
				t = ctx.STR(1).getText();
			} else
				s = ctx.STR(0).getText();
		} else if (ctx.TYPE() != null) {
			t = ctx.STR(0).getText();
		}
		result = s + "|" + t + "|" + p;
		return result;
	}

}
