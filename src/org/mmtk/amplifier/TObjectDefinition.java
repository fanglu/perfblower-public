package org.mmtk.amplifier;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class TObjectDefinition {
	String name;
	String include;
	String partition;
	int instanceSize;
	private List<Instance> instances;
	
	public TObjectDefinition() {
		instances = new ArrayList<Instance>();
		instanceSize = 0;
	}
	
	public int AddInstance(Instance i) {
		int tempSize = this.instanceSize;
		tempSize += Tools.getTypeValue(i.type);
		
		// -1 represents too large of Mischeader for Tobject
		if (tempSize > 32) return -1;
		
		this.instanceSize = tempSize;		
		instances.add(i);
		
		Collections.sort(instances, new InstancesComparator());
		
		return this.instanceSize;
	}
	
	public List<Instance> getInstances() {
		return this.instances;
	}
}
