package org.mmtk.amplifier;

import java.util.Comparator;

public class InstancesComparator implements Comparator<Instance>{

	@Override
	public int compare(Instance o1, Instance o2) {
		int i1 = Tools.getOrderValue(o1.type);
		int i2 = Tools.getOrderValue(o2.type);
		return i1 - i2;
	}

}
