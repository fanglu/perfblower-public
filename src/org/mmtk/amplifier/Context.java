//We don't need to generate this class
package org.mmtk.amplifier;

public class Context {
	public String name;
	public String seq;		
	public String type;		
	public Context next;	
	boolean isMain;			 

	public Context(String n, String s, String t, Context c, boolean b) {
		name = n;
		seq = s;
		type = t;
		next = c;
		isMain = b;
	}
}
