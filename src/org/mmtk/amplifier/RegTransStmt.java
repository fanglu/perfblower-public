package org.mmtk.amplifier;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegTransStmt {
			
		private Map<String, TObjectDefinition> tObjs = null;
		private String stmts = null;

		public RegTransStmt(Map<String, TObjectDefinition> tObjs, String stmts) {
			super();
			this.tObjs = tObjs;
			this.stmts = stmts;
		}
		
		public void transStmtByVar (String varName, TObjectDefinition tObj) {
			//String regEx1 = "getMetaData\\([\\s]*([a-zA-Z0-9]*)[\\s]*,[\\s]*([a-zA-Z0-9]*)[\\s]*\\)";	
			String regEx1 = "getMetaData(\\(.*\\))";
			Matcher m1 = Pattern.compile(regEx1).matcher(stmts);
		    StringBuffer sb = new StringBuffer(); 
			while(m1.find()) {
				ArrayList<String> parameters = getFunctionParameters(m1.group(1));
		        String obj = parameters.get(0);
		        String instanceName = parameters.get(1);
		        String remaining = parameters.get(2);
		        
		        // Determine if instance exists in tObj, error Otherwise
		        boolean invalidInstance = false;
		        Instance instance = null;
		        for (Instance i: tObj.getInstances()) {
		        	if (i.name.equals(instanceName)) {
		        		instance = i;
		        		invalidInstance = true;
		        		break;
		        	}
		        }
		        
		        if (!invalidInstance) {
		        	System.out.println("TObject does not contain instance");
		        	System.exit(0);
		        }
		        
		        // Recreate function call
		        m1.appendReplacement(sb
		        		, String.format("get%sMetaData(%s, %s)%s"
		        				, instance.type.substring(0, 1).toUpperCase() + instance.type.substring(1)
		        				, obj
		        				, instance.offset
		        				, remaining));
		   }
			m1.appendTail(sb);
			
			String regEx2 = "setMetaData(\\(.*\\))";
			Matcher m2 = Pattern.compile(regEx2).matcher(sb);
		    StringBuffer sb2 = new StringBuffer(); 

			while(m2.find()) {
				ArrayList<String> parameters = getFunctionParameters(m2.group(1));
				
		        String obj = parameters.get(0);
		        String instanceName = parameters.get(1);
		        String setVal = parameters.get(2);
		        String remaining = parameters.get(3);
		        
		        
		        // Determine if instance exists in tObj, error Otherwise
		        boolean invalidInstance = false;
		        Instance instance = null;
		        for (Instance i: tObj.getInstances()) {
		        	if (i.name.equals(instanceName)) {
		        		instance = i;
		        		invalidInstance = true;
		        		break;
		        	}
		        }
		        
		        if (!invalidInstance) {
		        	System.out.println("TObject does not contain instance");
		        	System.exit(0);
		        }
		        
		        // Recreate function call
		        m2.appendReplacement(sb2
		        		, String.format("set%sMetaData(%s, %s, %s)%s"
		        				, instance.type.substring(0, 1).toUpperCase() + instance.type.substring(1)
		        				, obj
		        				, instance.offset
		        				, setVal
		        				, remaining));
		        
			}
			
			m2.appendTail(sb2);
			setEvstmts(sb2.toString());
		}
		
		public String transStmt(){
			Iterator it = tObjs.entrySet().iterator();
			while (it.hasNext()) {
				Map.Entry entry = (Map.Entry) it.next();
				TObjectDefinition tObj = (TObjectDefinition)entry.getValue();
				transStmtByVar(entry.getKey().toString(), tObj);
			}
			return stmts;	
		}
		
		// This function is used to parse out the parameters in the function call
		public ArrayList<String> getFunctionParameters(String data) {
			ArrayList<String> strings = new ArrayList<String>();
			int balanced = 0;
			String parameter = "";
			// parse data for each string within the parameter declaration
			for (int i = 0; i < data.length(); i++) {
				if (data.charAt(i) == '(') {
					balanced++; 
					// this is still in the parameter, so add it to the string
					if (balanced > 1) parameter += data.charAt(i);
				}
				else if (data.charAt(i) == ')') {
					// this is still in the parameter, so add it to the string
					if (balanced > 1) parameter += data.charAt(i);
					balanced--;
				}
				// Next parameter only if it is in the currrent function
				else if (data.charAt(i) == ',' && balanced == 1) { 
					strings.add(parameter.trim());
					parameter = "";
				} else {
					parameter += data.charAt(i);
				}
				if (balanced == 0) {
					strings.add(parameter.trim());
					
					// Get the remaining data that exists (we don't want to lose this)
					parameter = "";
					if (i < data.length() - 1) {
						parameter = data.substring(i + 1, data.length());
					}
					strings.add(parameter);
					break;
				}
			}
			
			return strings;
		}
		
		
		public String getEvstmts() {
			return stmts;
		}
		public void setEvstmts(String evStmts) {
			this.stmts = evStmts;
		}
		
		public Map<String, TObjectDefinition> gettObjs() {
			return tObjs;
		}

		public void setInstances(Map<String, TObjectDefinition> tObjs) {
			this.tObjs = tObjs;
		}
		
		

	

}
