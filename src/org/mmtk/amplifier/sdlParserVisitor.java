package org.mmtk.amplifier;
// Generated from sdlParser.g4 by ANTLR 4.1
import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link sdlParserParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface sdlParserVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link sdlParserParser#innerCreator}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInnerCreator(@NotNull sdlParserParser.InnerCreatorContext ctx);

	/**
	 * Visit a parse tree produced by {@link sdlParserParser#genericMethodDeclaration}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitGenericMethodDeclaration(@NotNull sdlParserParser.GenericMethodDeclarationContext ctx);

	/**
	 * Visit a parse tree produced by {@link sdlParserParser#expressionList}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpressionList(@NotNull sdlParserParser.ExpressionListContext ctx);

	/**
	 * Visit a parse tree produced by {@link sdlParserParser#typeDeclaration}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTypeDeclaration(@NotNull sdlParserParser.TypeDeclarationContext ctx);

	/**
	 * Visit a parse tree produced by {@link sdlParserParser#forUpdate}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitForUpdate(@NotNull sdlParserParser.ForUpdateContext ctx);

	/**
	 * Visit a parse tree produced by {@link sdlParserParser#annotation}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAnnotation(@NotNull sdlParserParser.AnnotationContext ctx);

	/**
	 * Visit a parse tree produced by {@link sdlParserParser#enumConstant}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEnumConstant(@NotNull sdlParserParser.EnumConstantContext ctx);

	/**
	 * Visit a parse tree produced by {@link sdlParserParser#SPT}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSPT(@NotNull sdlParserParser.SPTContext ctx);

	/**
	 * Visit a parse tree produced by {@link sdlParserParser#importDeclaration}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitImportDeclaration(@NotNull sdlParserParser.ImportDeclarationContext ctx);

	/**
	 * Visit a parse tree produced by {@link sdlParserParser#annotationMethodOrConstantRest}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAnnotationMethodOrConstantRest(@NotNull sdlParserParser.AnnotationMethodOrConstantRestContext ctx);

	/**
	 * Visit a parse tree produced by {@link sdlParserParser#instDef}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInstDef(@NotNull sdlParserParser.InstDefContext ctx);

	/**
	 * Visit a parse tree produced by {@link sdlParserParser#enumConstantName}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEnumConstantName(@NotNull sdlParserParser.EnumConstantNameContext ctx);

	/**
	 * Visit a parse tree produced by {@link sdlParserParser#variableDeclarators}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVariableDeclarators(@NotNull sdlParserParser.VariableDeclaratorsContext ctx);

	/**
	 * Visit a parse tree produced by {@link sdlParserParser#elementValuePairs}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitElementValuePairs(@NotNull sdlParserParser.ElementValuePairsContext ctx);

	/**
	 * Visit a parse tree produced by {@link sdlParserParser#hb}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitHb(@NotNull sdlParserParser.HbContext ctx);

	/**
	 * Visit a parse tree produced by {@link sdlParserParser#switchBlock}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSwitchBlock(@NotNull sdlParserParser.SwitchBlockContext ctx);

	/**
	 * Visit a parse tree produced by {@link sdlParserParser#interfaceBodyDeclaration}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInterfaceBodyDeclaration(@NotNull sdlParserParser.InterfaceBodyDeclarationContext ctx);

	/**
	 * Visit a parse tree produced by {@link sdlParserParser#catchClause}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCatchClause(@NotNull sdlParserParser.CatchClauseContext ctx);

	/**
	 * Visit a parse tree produced by {@link sdlParserParser#enumConstants}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEnumConstants(@NotNull sdlParserParser.EnumConstantsContext ctx);

	/**
	 * Visit a parse tree produced by {@link sdlParserParser#constantExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitConstantExpression(@NotNull sdlParserParser.ConstantExpressionContext ctx);

	/**
	 * Visit a parse tree produced by {@link sdlParserParser#pb}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPb(@NotNull sdlParserParser.PbContext ctx);

	/**
	 * Visit a parse tree produced by {@link sdlParserParser#enumBody}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEnumBody(@NotNull sdlParserParser.EnumBodyContext ctx);

	/**
	 * Visit a parse tree produced by {@link sdlParserParser#enumDeclaration}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEnumDeclaration(@NotNull sdlParserParser.EnumDeclarationContext ctx);

	/**
	 * Visit a parse tree produced by {@link sdlParserParser#pk}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPk(@NotNull sdlParserParser.PkContext ctx);

	/**
	 * Visit a parse tree produced by {@link sdlParserParser#typeParameter}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTypeParameter(@NotNull sdlParserParser.TypeParameterContext ctx);

	/**
	 * Visit a parse tree produced by {@link sdlParserParser#explicitConstructorInvocation}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExplicitConstructorInvocation(@NotNull sdlParserParser.ExplicitConstructorInvocationContext ctx);

	/**
	 * Visit a parse tree produced by {@link sdlParserParser#enumBodyDeclarations}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEnumBodyDeclarations(@NotNull sdlParserParser.EnumBodyDeclarationsContext ctx);

	/**
	 * Visit a parse tree produced by {@link sdlParserParser#interfaceMethodDeclaratorRest}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInterfaceMethodDeclaratorRest(@NotNull sdlParserParser.InterfaceMethodDeclaratorRestContext ctx);

	/**
	 * Visit a parse tree produced by {@link sdlParserParser#typeBound}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTypeBound(@NotNull sdlParserParser.TypeBoundContext ctx);

	/**
	 * Visit a parse tree produced by {@link sdlParserParser#statementExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStatementExpression(@NotNull sdlParserParser.StatementExpressionContext ctx);

	/**
	 * Visit a parse tree produced by {@link sdlParserParser#block}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBlock(@NotNull sdlParserParser.BlockContext ctx);

	/**
	 * Visit a parse tree produced by {@link sdlParserParser#variableInitializer}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVariableInitializer(@NotNull sdlParserParser.VariableInitializerContext ctx);

	/**
	 * Visit a parse tree produced by {@link sdlParserParser#eb}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEb(@NotNull sdlParserParser.EbContext ctx);

	/**
	 * Visit a parse tree produced by {@link sdlParserParser#constantDeclaratorsRest}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitConstantDeclaratorsRest(@NotNull sdlParserParser.ConstantDeclaratorsRestContext ctx);

	/**
	 * Visit a parse tree produced by {@link sdlParserParser#localVariableDeclarationStatement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLocalVariableDeclarationStatement(@NotNull sdlParserParser.LocalVariableDeclarationStatementContext ctx);

	/**
	 * Visit a parse tree produced by {@link sdlParserParser#interfaceMethodOrFieldDecl}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInterfaceMethodOrFieldDecl(@NotNull sdlParserParser.InterfaceMethodOrFieldDeclContext ctx);

	/**
	 * Visit a parse tree produced by {@link sdlParserParser#fieldDeclaration}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFieldDeclaration(@NotNull sdlParserParser.FieldDeclarationContext ctx);

	/**
	 * Visit a parse tree produced by {@link sdlParserParser#normalInterfaceDeclaration}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNormalInterfaceDeclaration(@NotNull sdlParserParser.NormalInterfaceDeclarationContext ctx);

	/**
	 * Visit a parse tree produced by {@link sdlParserParser#modifiers}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitModifiers(@NotNull sdlParserParser.ModifiersContext ctx);

	/**
	 * Visit a parse tree produced by {@link sdlParserParser#explicitGenericInvocation}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExplicitGenericInvocation(@NotNull sdlParserParser.ExplicitGenericInvocationContext ctx);

	/**
	 * Visit a parse tree produced by {@link sdlParserParser#member}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMember(@NotNull sdlParserParser.MemberContext ctx);

	/**
	 * Visit a parse tree produced by {@link sdlParserParser#parExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParExpression(@NotNull sdlParserParser.ParExpressionContext ctx);

	/**
	 * Visit a parse tree produced by {@link sdlParserParser#catches}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCatches(@NotNull sdlParserParser.CatchesContext ctx);

	/**
	 * Visit a parse tree produced by {@link sdlParserParser#switchLabel}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSwitchLabel(@NotNull sdlParserParser.SwitchLabelContext ctx);

	/**
	 * Visit a parse tree produced by {@link sdlParserParser#e}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitE(@NotNull sdlParserParser.EContext ctx);

	/**
	 * Visit a parse tree produced by {@link sdlParserParser#typeParameters}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTypeParameters(@NotNull sdlParserParser.TypeParametersContext ctx);

	/**
	 * Visit a parse tree produced by {@link sdlParserParser#c}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitC(@NotNull sdlParserParser.CContext ctx);

	/**
	 * Visit a parse tree produced by {@link sdlParserParser#qualifiedName}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitQualifiedName(@NotNull sdlParserParser.QualifiedNameContext ctx);

	/**
	 * Visit a parse tree produced by {@link sdlParserParser#classDeclaration}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitClassDeclaration(@NotNull sdlParserParser.ClassDeclarationContext ctx);

	/**
	 * Visit a parse tree produced by {@link sdlParserParser#o}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitO(@NotNull sdlParserParser.OContext ctx);

	/**
	 * Visit a parse tree produced by {@link sdlParserParser#booleanLiteral}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBooleanLiteral(@NotNull sdlParserParser.BooleanLiteralContext ctx);

	/**
	 * Visit a parse tree produced by {@link sdlParserParser#annotationConstantRest}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAnnotationConstantRest(@NotNull sdlParserParser.AnnotationConstantRestContext ctx);

	/**
	 * Visit a parse tree produced by {@link sdlParserParser#fd}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFd(@NotNull sdlParserParser.FdContext ctx);

	/**
	 * Visit a parse tree produced by {@link sdlParserParser#h}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitH(@NotNull sdlParserParser.HContext ctx);

	/**
	 * Visit a parse tree produced by {@link sdlParserParser#typeName}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTypeName(@NotNull sdlParserParser.TypeNameContext ctx);

	/**
	 * Visit a parse tree produced by {@link sdlParserParser#arguments}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArguments(@NotNull sdlParserParser.ArgumentsContext ctx);

	/**
	 * Visit a parse tree produced by {@link sdlParserParser#constructorBody}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitConstructorBody(@NotNull sdlParserParser.ConstructorBodyContext ctx);

	/**
	 * Visit a parse tree produced by {@link sdlParserParser#TSP}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTSP(@NotNull sdlParserParser.TSPContext ctx);

	/**
	 * Visit a parse tree produced by {@link sdlParserParser#s}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitS(@NotNull sdlParserParser.SContext ctx);

	/**
	 * Visit a parse tree produced by {@link sdlParserParser#ek}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEk(@NotNull sdlParserParser.EkContext ctx);

	/**
	 * Visit a parse tree produced by {@link sdlParserParser#formalParameters}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFormalParameters(@NotNull sdlParserParser.FormalParametersContext ctx);

	/**
	 * Visit a parse tree produced by {@link sdlParserParser#p}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitP(@NotNull sdlParserParser.PContext ctx);

	/**
	 * Visit a parse tree produced by {@link sdlParserParser#typeArgument}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTypeArgument(@NotNull sdlParserParser.TypeArgumentContext ctx);

	/**
	 * Visit a parse tree produced by {@link sdlParserParser#forInit}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitForInit(@NotNull sdlParserParser.ForInitContext ctx);

	/**
	 * Visit a parse tree produced by {@link sdlParserParser#annotations}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAnnotations(@NotNull sdlParserParser.AnnotationsContext ctx);

	/**
	 * Visit a parse tree produced by {@link sdlParserParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpression(@NotNull sdlParserParser.ExpressionContext ctx);

	/**
	 * Visit a parse tree produced by {@link sdlParserParser#variableDeclarator}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVariableDeclarator(@NotNull sdlParserParser.VariableDeclaratorContext ctx);

	/**
	 * Visit a parse tree produced by {@link sdlParserParser#annotationTypeDeclaration}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAnnotationTypeDeclaration(@NotNull sdlParserParser.AnnotationTypeDeclarationContext ctx);

	/**
	 * Visit a parse tree produced by {@link sdlParserParser#formalParameter}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFormalParameter(@NotNull sdlParserParser.FormalParameterContext ctx);

	/**
	 * Visit a parse tree produced by {@link sdlParserParser#type}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitType(@NotNull sdlParserParser.TypeContext ctx);

	/**
	 * Visit a parse tree produced by {@link sdlParserParser#elementValueArrayInitializer}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitElementValueArrayInitializer(@NotNull sdlParserParser.ElementValueArrayInitializerContext ctx);

	/**
	 * Visit a parse tree produced by {@link sdlParserParser#annotationName}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAnnotationName(@NotNull sdlParserParser.AnnotationNameContext ctx);

	/**
	 * Visit a parse tree produced by {@link sdlParserParser#tb}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTb(@NotNull sdlParserParser.TbContext ctx);

	/**
	 * Visit a parse tree produced by {@link sdlParserParser#enhancedForControl}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEnhancedForControl(@NotNull sdlParserParser.EnhancedForControlContext ctx);

	/**
	 * Visit a parse tree produced by {@link sdlParserParser#annotationMethodRest}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAnnotationMethodRest(@NotNull sdlParserParser.AnnotationMethodRestContext ctx);

	/**
	 * Visit a parse tree produced by {@link sdlParserParser#formalParameterDeclsRest}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFormalParameterDeclsRest(@NotNull sdlParserParser.FormalParameterDeclsRestContext ctx);

	/**
	 * Visit a parse tree produced by {@link sdlParserParser#primary}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPrimary(@NotNull sdlParserParser.PrimaryContext ctx);

	/**
	 * Visit a parse tree produced by {@link sdlParserParser#classBody}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitClassBody(@NotNull sdlParserParser.ClassBodyContext ctx);

	/**
	 * Visit a parse tree produced by {@link sdlParserParser#defaultValue}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDefaultValue(@NotNull sdlParserParser.DefaultValueContext ctx);

	/**
	 * Visit a parse tree produced by {@link sdlParserParser#classOrInterfaceModifier}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitClassOrInterfaceModifier(@NotNull sdlParserParser.ClassOrInterfaceModifierContext ctx);

	/**
	 * Visit a parse tree produced by {@link sdlParserParser#TPS}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTPS(@NotNull sdlParserParser.TPSContext ctx);

	/**
	 * Visit a parse tree produced by {@link sdlParserParser#variableModifier}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVariableModifier(@NotNull sdlParserParser.VariableModifierContext ctx);

	/**
	 * Visit a parse tree produced by {@link sdlParserParser#createdName}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCreatedName(@NotNull sdlParserParser.CreatedNameContext ctx);

	/**
	 * Visit a parse tree produced by {@link sdlParserParser#interfaceDeclaration}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInterfaceDeclaration(@NotNull sdlParserParser.InterfaceDeclarationContext ctx);

	/**
	 * Visit a parse tree produced by {@link sdlParserParser#packageDeclaration}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPackageDeclaration(@NotNull sdlParserParser.PackageDeclarationContext ctx);

	/**
	 * Visit a parse tree produced by {@link sdlParserParser#PST}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPST(@NotNull sdlParserParser.PSTContext ctx);

	/**
	 * Visit a parse tree produced by {@link sdlParserParser#constantDeclarator}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitConstantDeclarator(@NotNull sdlParserParser.ConstantDeclaratorContext ctx);

	/**
	 * Visit a parse tree produced by {@link sdlParserParser#constantDeclaratorRest}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitConstantDeclaratorRest(@NotNull sdlParserParser.ConstantDeclaratorRestContext ctx);

	/**
	 * Visit a parse tree produced by {@link sdlParserParser#typeArguments}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTypeArguments(@NotNull sdlParserParser.TypeArgumentsContext ctx);

	/**
	 * Visit a parse tree produced by {@link sdlParserParser#classCreatorRest}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitClassCreatorRest(@NotNull sdlParserParser.ClassCreatorRestContext ctx);

	/**
	 * Visit a parse tree produced by {@link sdlParserParser#statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStatement(@NotNull sdlParserParser.StatementContext ctx);

	/**
	 * Visit a parse tree produced by {@link sdlParserParser#modifier}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitModifier(@NotNull sdlParserParser.ModifierContext ctx);

	/**
	 * Visit a parse tree produced by {@link sdlParserParser#interfaceBody}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInterfaceBody(@NotNull sdlParserParser.InterfaceBodyContext ctx);

	/**
	 * Visit a parse tree produced by {@link sdlParserParser#classBodyDeclaration}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitClassBodyDeclaration(@NotNull sdlParserParser.ClassBodyDeclarationContext ctx);

	/**
	 * Visit a parse tree produced by {@link sdlParserParser#packageOrTypeName}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPackageOrTypeName(@NotNull sdlParserParser.PackageOrTypeNameContext ctx);

	/**
	 * Visit a parse tree produced by {@link sdlParserParser#forControl}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitForControl(@NotNull sdlParserParser.ForControlContext ctx);

	/**
	 * Visit a parse tree produced by {@link sdlParserParser#localVariableDeclaration}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLocalVariableDeclaration(@NotNull sdlParserParser.LocalVariableDeclarationContext ctx);

	/**
	 * Visit a parse tree produced by {@link sdlParserParser#typeList}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTypeList(@NotNull sdlParserParser.TypeListContext ctx);

	/**
	 * Visit a parse tree produced by {@link sdlParserParser#STP}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSTP(@NotNull sdlParserParser.STPContext ctx);

	/**
	 * Visit a parse tree produced by {@link sdlParserParser#variableDeclaratorId}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVariableDeclaratorId(@NotNull sdlParserParser.VariableDeclaratorIdContext ctx);

	/**
	 * Visit a parse tree produced by {@link sdlParserParser#compilationUnit}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCompilationUnit(@NotNull sdlParserParser.CompilationUnitContext ctx);

	/**
	 * Visit a parse tree produced by {@link sdlParserParser#elementValue}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitElementValue(@NotNull sdlParserParser.ElementValueContext ctx);

	/**
	 * Visit a parse tree produced by {@link sdlParserParser#classOrInterfaceType}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitClassOrInterfaceType(@NotNull sdlParserParser.ClassOrInterfaceTypeContext ctx);

	/**
	 * Visit a parse tree produced by {@link sdlParserParser#blockStatement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBlockStatement(@NotNull sdlParserParser.BlockStatementContext ctx);

	/**
	 * Visit a parse tree produced by {@link sdlParserParser#annotationTypeElementDeclaration}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAnnotationTypeElementDeclaration(@NotNull sdlParserParser.AnnotationTypeElementDeclarationContext ctx);

	/**
	 * Visit a parse tree produced by {@link sdlParserParser#annotationTypeBody}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAnnotationTypeBody(@NotNull sdlParserParser.AnnotationTypeBodyContext ctx);

	/**
	 * Visit a parse tree produced by {@link sdlParserParser#interfaceMemberDecl}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInterfaceMemberDecl(@NotNull sdlParserParser.InterfaceMemberDeclContext ctx);

	/**
	 * Visit a parse tree produced by {@link sdlParserParser#integerLiteral}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIntegerLiteral(@NotNull sdlParserParser.IntegerLiteralContext ctx);

	/**
	 * Visit a parse tree produced by {@link sdlParserParser#creator}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCreator(@NotNull sdlParserParser.CreatorContext ctx);

	/**
	 * Visit a parse tree produced by {@link sdlParserParser#qualifiedNameList}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitQualifiedNameList(@NotNull sdlParserParser.QualifiedNameListContext ctx);

	/**
	 * Visit a parse tree produced by {@link sdlParserParser#interfaceGenericMethodDecl}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInterfaceGenericMethodDecl(@NotNull sdlParserParser.InterfaceGenericMethodDeclContext ctx);

	/**
	 * Visit a parse tree produced by {@link sdlParserParser#formalParameterDecls}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFormalParameterDecls(@NotNull sdlParserParser.FormalParameterDeclsContext ctx);

	/**
	 * Visit a parse tree produced by {@link sdlParserParser#voidMethodDeclaratorRest}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVoidMethodDeclaratorRest(@NotNull sdlParserParser.VoidMethodDeclaratorRestContext ctx);

	/**
	 * Visit a parse tree produced by {@link sdlParserParser#PTS}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPTS(@NotNull sdlParserParser.PTSContext ctx);

	/**
	 * Visit a parse tree produced by {@link sdlParserParser#methodDeclaration}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMethodDeclaration(@NotNull sdlParserParser.MethodDeclarationContext ctx);

	/**
	 * Visit a parse tree produced by {@link sdlParserParser#annotationTypeElementRest}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAnnotationTypeElementRest(@NotNull sdlParserParser.AnnotationTypeElementRestContext ctx);

	/**
	 * Visit a parse tree produced by {@link sdlParserParser#methodDeclarationRest}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMethodDeclarationRest(@NotNull sdlParserParser.MethodDeclarationRestContext ctx);

	/**
	 * Visit a parse tree produced by {@link sdlParserParser#constructorDeclaration}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitConstructorDeclaration(@NotNull sdlParserParser.ConstructorDeclarationContext ctx);

	/**
	 * Visit a parse tree produced by {@link sdlParserParser#voidInterfaceMethodDeclaratorRest}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVoidInterfaceMethodDeclaratorRest(@NotNull sdlParserParser.VoidInterfaceMethodDeclaratorRestContext ctx);

	/**
	 * Visit a parse tree produced by {@link sdlParserParser#elementValuePair}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitElementValuePair(@NotNull sdlParserParser.ElementValuePairContext ctx);

	/**
	 * Visit a parse tree produced by {@link sdlParserParser#interfaceMethodOrFieldRest}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInterfaceMethodOrFieldRest(@NotNull sdlParserParser.InterfaceMethodOrFieldRestContext ctx);

	/**
	 * Visit a parse tree produced by {@link sdlParserParser#methodBody}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMethodBody(@NotNull sdlParserParser.MethodBodyContext ctx);

	/**
	 * Visit a parse tree produced by {@link sdlParserParser#arrayInitializer}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArrayInitializer(@NotNull sdlParserParser.ArrayInitializerContext ctx);

	/**
	 * Visit a parse tree produced by {@link sdlParserParser#variableModifiers}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVariableModifiers(@NotNull sdlParserParser.VariableModifiersContext ctx);

	/**
	 * Visit a parse tree produced by {@link sdlParserParser#primitiveType}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPrimitiveType(@NotNull sdlParserParser.PrimitiveTypeContext ctx);

	/**
	 * Visit a parse tree produced by {@link sdlParserParser#nonWildcardTypeArguments}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNonWildcardTypeArguments(@NotNull sdlParserParser.NonWildcardTypeArgumentsContext ctx);

	/**
	 * Visit a parse tree produced by {@link sdlParserParser#arrayCreatorRest}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArrayCreatorRest(@NotNull sdlParserParser.ArrayCreatorRestContext ctx);

	/**
	 * Visit a parse tree produced by {@link sdlParserParser#switchBlockStatementGroup}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSwitchBlockStatementGroup(@NotNull sdlParserParser.SwitchBlockStatementGroupContext ctx);

	/**
	 * Visit a parse tree produced by {@link sdlParserParser#literal}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLiteral(@NotNull sdlParserParser.LiteralContext ctx);
}