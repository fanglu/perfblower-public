package org.mmtk.amplifier;

public class UseHistory {
	public static String kind = "all";
	int num_of_element = 0;
	boolean[] array = new boolean[10];

	public boolean isFull() {
		if (array.length == num_of_element)
			return true;
		else
			return false;
	}

	public void update(boolean x) {
		if (isFull()) {
			for (int i = 0; i < num_of_element - 1; ++i)
				array[i] = array[i + 1];
			array[num_of_element - 1] = x;
		} else {
			array[num_of_element - 1] = x;
			++num_of_element;
		}
	}

	public boolean get(int x) {
		return array[x];
	}

	public boolean contains(boolean x) {
		for (int i = 0; i < num_of_element; ++i) {
			if (x == array[i]) {
				return true;
			}
		}
		return false;
	}

	public boolean containsSameValue() {
		if (num_of_element == 0) {
			return false;
		} else {
			boolean first = array[0];
			for (int i = 1; i < num_of_element; ++i) {
				if (array[i] != first)
					return false;
			}
			return true;
		}
	}

	public UseHistory subHistory(int start, int length) {
		UseHistory result = new UseHistory();
		result.array = this.array;
		result.num_of_element = length;
		return result;
	}

	public int length() {
		return num_of_element;
	}
}
