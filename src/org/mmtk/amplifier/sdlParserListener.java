package org.mmtk.amplifier;
// Generated from sdlParser.g4 by ANTLR 4.1
import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link sdlParserParser}.
 */
public interface sdlParserListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link sdlParserParser#innerCreator}.
	 * @param ctx the parse tree
	 */
	void enterInnerCreator(@NotNull sdlParserParser.InnerCreatorContext ctx);
	/**
	 * Exit a parse tree produced by {@link sdlParserParser#innerCreator}.
	 * @param ctx the parse tree
	 */
	void exitInnerCreator(@NotNull sdlParserParser.InnerCreatorContext ctx);

	/**
	 * Enter a parse tree produced by {@link sdlParserParser#genericMethodDeclaration}.
	 * @param ctx the parse tree
	 */
	void enterGenericMethodDeclaration(@NotNull sdlParserParser.GenericMethodDeclarationContext ctx);
	/**
	 * Exit a parse tree produced by {@link sdlParserParser#genericMethodDeclaration}.
	 * @param ctx the parse tree
	 */
	void exitGenericMethodDeclaration(@NotNull sdlParserParser.GenericMethodDeclarationContext ctx);

	/**
	 * Enter a parse tree produced by {@link sdlParserParser#expressionList}.
	 * @param ctx the parse tree
	 */
	void enterExpressionList(@NotNull sdlParserParser.ExpressionListContext ctx);
	/**
	 * Exit a parse tree produced by {@link sdlParserParser#expressionList}.
	 * @param ctx the parse tree
	 */
	void exitExpressionList(@NotNull sdlParserParser.ExpressionListContext ctx);

	/**
	 * Enter a parse tree produced by {@link sdlParserParser#typeDeclaration}.
	 * @param ctx the parse tree
	 */
	void enterTypeDeclaration(@NotNull sdlParserParser.TypeDeclarationContext ctx);
	/**
	 * Exit a parse tree produced by {@link sdlParserParser#typeDeclaration}.
	 * @param ctx the parse tree
	 */
	void exitTypeDeclaration(@NotNull sdlParserParser.TypeDeclarationContext ctx);

	/**
	 * Enter a parse tree produced by {@link sdlParserParser#forUpdate}.
	 * @param ctx the parse tree
	 */
	void enterForUpdate(@NotNull sdlParserParser.ForUpdateContext ctx);
	/**
	 * Exit a parse tree produced by {@link sdlParserParser#forUpdate}.
	 * @param ctx the parse tree
	 */
	void exitForUpdate(@NotNull sdlParserParser.ForUpdateContext ctx);

	/**
	 * Enter a parse tree produced by {@link sdlParserParser#annotation}.
	 * @param ctx the parse tree
	 */
	void enterAnnotation(@NotNull sdlParserParser.AnnotationContext ctx);
	/**
	 * Exit a parse tree produced by {@link sdlParserParser#annotation}.
	 * @param ctx the parse tree
	 */
	void exitAnnotation(@NotNull sdlParserParser.AnnotationContext ctx);

	/**
	 * Enter a parse tree produced by {@link sdlParserParser#enumConstant}.
	 * @param ctx the parse tree
	 */
	void enterEnumConstant(@NotNull sdlParserParser.EnumConstantContext ctx);
	/**
	 * Exit a parse tree produced by {@link sdlParserParser#enumConstant}.
	 * @param ctx the parse tree
	 */
	void exitEnumConstant(@NotNull sdlParserParser.EnumConstantContext ctx);

	/**
	 * Enter a parse tree produced by {@link sdlParserParser#SPT}.
	 * @param ctx the parse tree
	 */
	void enterSPT(@NotNull sdlParserParser.SPTContext ctx);
	/**
	 * Exit a parse tree produced by {@link sdlParserParser#SPT}.
	 * @param ctx the parse tree
	 */
	void exitSPT(@NotNull sdlParserParser.SPTContext ctx);

	/**
	 * Enter a parse tree produced by {@link sdlParserParser#importDeclaration}.
	 * @param ctx the parse tree
	 */
	void enterImportDeclaration(@NotNull sdlParserParser.ImportDeclarationContext ctx);
	/**
	 * Exit a parse tree produced by {@link sdlParserParser#importDeclaration}.
	 * @param ctx the parse tree
	 */
	void exitImportDeclaration(@NotNull sdlParserParser.ImportDeclarationContext ctx);

	/**
	 * Enter a parse tree produced by {@link sdlParserParser#annotationMethodOrConstantRest}.
	 * @param ctx the parse tree
	 */
	void enterAnnotationMethodOrConstantRest(@NotNull sdlParserParser.AnnotationMethodOrConstantRestContext ctx);
	/**
	 * Exit a parse tree produced by {@link sdlParserParser#annotationMethodOrConstantRest}.
	 * @param ctx the parse tree
	 */
	void exitAnnotationMethodOrConstantRest(@NotNull sdlParserParser.AnnotationMethodOrConstantRestContext ctx);

	/**
	 * Enter a parse tree produced by {@link sdlParserParser#instDef}.
	 * @param ctx the parse tree
	 */
	void enterInstDef(@NotNull sdlParserParser.InstDefContext ctx);
	/**
	 * Exit a parse tree produced by {@link sdlParserParser#instDef}.
	 * @param ctx the parse tree
	 */
	void exitInstDef(@NotNull sdlParserParser.InstDefContext ctx);

	/**
	 * Enter a parse tree produced by {@link sdlParserParser#enumConstantName}.
	 * @param ctx the parse tree
	 */
	void enterEnumConstantName(@NotNull sdlParserParser.EnumConstantNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link sdlParserParser#enumConstantName}.
	 * @param ctx the parse tree
	 */
	void exitEnumConstantName(@NotNull sdlParserParser.EnumConstantNameContext ctx);

	/**
	 * Enter a parse tree produced by {@link sdlParserParser#variableDeclarators}.
	 * @param ctx the parse tree
	 */
	void enterVariableDeclarators(@NotNull sdlParserParser.VariableDeclaratorsContext ctx);
	/**
	 * Exit a parse tree produced by {@link sdlParserParser#variableDeclarators}.
	 * @param ctx the parse tree
	 */
	void exitVariableDeclarators(@NotNull sdlParserParser.VariableDeclaratorsContext ctx);

	/**
	 * Enter a parse tree produced by {@link sdlParserParser#elementValuePairs}.
	 * @param ctx the parse tree
	 */
	void enterElementValuePairs(@NotNull sdlParserParser.ElementValuePairsContext ctx);
	/**
	 * Exit a parse tree produced by {@link sdlParserParser#elementValuePairs}.
	 * @param ctx the parse tree
	 */
	void exitElementValuePairs(@NotNull sdlParserParser.ElementValuePairsContext ctx);

	/**
	 * Enter a parse tree produced by {@link sdlParserParser#hb}.
	 * @param ctx the parse tree
	 */
	void enterHb(@NotNull sdlParserParser.HbContext ctx);
	/**
	 * Exit a parse tree produced by {@link sdlParserParser#hb}.
	 * @param ctx the parse tree
	 */
	void exitHb(@NotNull sdlParserParser.HbContext ctx);

	/**
	 * Enter a parse tree produced by {@link sdlParserParser#switchBlock}.
	 * @param ctx the parse tree
	 */
	void enterSwitchBlock(@NotNull sdlParserParser.SwitchBlockContext ctx);
	/**
	 * Exit a parse tree produced by {@link sdlParserParser#switchBlock}.
	 * @param ctx the parse tree
	 */
	void exitSwitchBlock(@NotNull sdlParserParser.SwitchBlockContext ctx);

	/**
	 * Enter a parse tree produced by {@link sdlParserParser#interfaceBodyDeclaration}.
	 * @param ctx the parse tree
	 */
	void enterInterfaceBodyDeclaration(@NotNull sdlParserParser.InterfaceBodyDeclarationContext ctx);
	/**
	 * Exit a parse tree produced by {@link sdlParserParser#interfaceBodyDeclaration}.
	 * @param ctx the parse tree
	 */
	void exitInterfaceBodyDeclaration(@NotNull sdlParserParser.InterfaceBodyDeclarationContext ctx);

	/**
	 * Enter a parse tree produced by {@link sdlParserParser#catchClause}.
	 * @param ctx the parse tree
	 */
	void enterCatchClause(@NotNull sdlParserParser.CatchClauseContext ctx);
	/**
	 * Exit a parse tree produced by {@link sdlParserParser#catchClause}.
	 * @param ctx the parse tree
	 */
	void exitCatchClause(@NotNull sdlParserParser.CatchClauseContext ctx);

	/**
	 * Enter a parse tree produced by {@link sdlParserParser#enumConstants}.
	 * @param ctx the parse tree
	 */
	void enterEnumConstants(@NotNull sdlParserParser.EnumConstantsContext ctx);
	/**
	 * Exit a parse tree produced by {@link sdlParserParser#enumConstants}.
	 * @param ctx the parse tree
	 */
	void exitEnumConstants(@NotNull sdlParserParser.EnumConstantsContext ctx);

	/**
	 * Enter a parse tree produced by {@link sdlParserParser#constantExpression}.
	 * @param ctx the parse tree
	 */
	void enterConstantExpression(@NotNull sdlParserParser.ConstantExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link sdlParserParser#constantExpression}.
	 * @param ctx the parse tree
	 */
	void exitConstantExpression(@NotNull sdlParserParser.ConstantExpressionContext ctx);

	/**
	 * Enter a parse tree produced by {@link sdlParserParser#pb}.
	 * @param ctx the parse tree
	 */
	void enterPb(@NotNull sdlParserParser.PbContext ctx);
	/**
	 * Exit a parse tree produced by {@link sdlParserParser#pb}.
	 * @param ctx the parse tree
	 */
	void exitPb(@NotNull sdlParserParser.PbContext ctx);

	/**
	 * Enter a parse tree produced by {@link sdlParserParser#enumBody}.
	 * @param ctx the parse tree
	 */
	void enterEnumBody(@NotNull sdlParserParser.EnumBodyContext ctx);
	/**
	 * Exit a parse tree produced by {@link sdlParserParser#enumBody}.
	 * @param ctx the parse tree
	 */
	void exitEnumBody(@NotNull sdlParserParser.EnumBodyContext ctx);

	/**
	 * Enter a parse tree produced by {@link sdlParserParser#enumDeclaration}.
	 * @param ctx the parse tree
	 */
	void enterEnumDeclaration(@NotNull sdlParserParser.EnumDeclarationContext ctx);
	/**
	 * Exit a parse tree produced by {@link sdlParserParser#enumDeclaration}.
	 * @param ctx the parse tree
	 */
	void exitEnumDeclaration(@NotNull sdlParserParser.EnumDeclarationContext ctx);

	/**
	 * Enter a parse tree produced by {@link sdlParserParser#pk}.
	 * @param ctx the parse tree
	 */
	void enterPk(@NotNull sdlParserParser.PkContext ctx);
	/**
	 * Exit a parse tree produced by {@link sdlParserParser#pk}.
	 * @param ctx the parse tree
	 */
	void exitPk(@NotNull sdlParserParser.PkContext ctx);

	/**
	 * Enter a parse tree produced by {@link sdlParserParser#typeParameter}.
	 * @param ctx the parse tree
	 */
	void enterTypeParameter(@NotNull sdlParserParser.TypeParameterContext ctx);
	/**
	 * Exit a parse tree produced by {@link sdlParserParser#typeParameter}.
	 * @param ctx the parse tree
	 */
	void exitTypeParameter(@NotNull sdlParserParser.TypeParameterContext ctx);

	/**
	 * Enter a parse tree produced by {@link sdlParserParser#explicitConstructorInvocation}.
	 * @param ctx the parse tree
	 */
	void enterExplicitConstructorInvocation(@NotNull sdlParserParser.ExplicitConstructorInvocationContext ctx);
	/**
	 * Exit a parse tree produced by {@link sdlParserParser#explicitConstructorInvocation}.
	 * @param ctx the parse tree
	 */
	void exitExplicitConstructorInvocation(@NotNull sdlParserParser.ExplicitConstructorInvocationContext ctx);

	/**
	 * Enter a parse tree produced by {@link sdlParserParser#enumBodyDeclarations}.
	 * @param ctx the parse tree
	 */
	void enterEnumBodyDeclarations(@NotNull sdlParserParser.EnumBodyDeclarationsContext ctx);
	/**
	 * Exit a parse tree produced by {@link sdlParserParser#enumBodyDeclarations}.
	 * @param ctx the parse tree
	 */
	void exitEnumBodyDeclarations(@NotNull sdlParserParser.EnumBodyDeclarationsContext ctx);

	/**
	 * Enter a parse tree produced by {@link sdlParserParser#interfaceMethodDeclaratorRest}.
	 * @param ctx the parse tree
	 */
	void enterInterfaceMethodDeclaratorRest(@NotNull sdlParserParser.InterfaceMethodDeclaratorRestContext ctx);
	/**
	 * Exit a parse tree produced by {@link sdlParserParser#interfaceMethodDeclaratorRest}.
	 * @param ctx the parse tree
	 */
	void exitInterfaceMethodDeclaratorRest(@NotNull sdlParserParser.InterfaceMethodDeclaratorRestContext ctx);

	/**
	 * Enter a parse tree produced by {@link sdlParserParser#typeBound}.
	 * @param ctx the parse tree
	 */
	void enterTypeBound(@NotNull sdlParserParser.TypeBoundContext ctx);
	/**
	 * Exit a parse tree produced by {@link sdlParserParser#typeBound}.
	 * @param ctx the parse tree
	 */
	void exitTypeBound(@NotNull sdlParserParser.TypeBoundContext ctx);

	/**
	 * Enter a parse tree produced by {@link sdlParserParser#statementExpression}.
	 * @param ctx the parse tree
	 */
	void enterStatementExpression(@NotNull sdlParserParser.StatementExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link sdlParserParser#statementExpression}.
	 * @param ctx the parse tree
	 */
	void exitStatementExpression(@NotNull sdlParserParser.StatementExpressionContext ctx);

	/**
	 * Enter a parse tree produced by {@link sdlParserParser#block}.
	 * @param ctx the parse tree
	 */
	void enterBlock(@NotNull sdlParserParser.BlockContext ctx);
	/**
	 * Exit a parse tree produced by {@link sdlParserParser#block}.
	 * @param ctx the parse tree
	 */
	void exitBlock(@NotNull sdlParserParser.BlockContext ctx);

	/**
	 * Enter a parse tree produced by {@link sdlParserParser#variableInitializer}.
	 * @param ctx the parse tree
	 */
	void enterVariableInitializer(@NotNull sdlParserParser.VariableInitializerContext ctx);
	/**
	 * Exit a parse tree produced by {@link sdlParserParser#variableInitializer}.
	 * @param ctx the parse tree
	 */
	void exitVariableInitializer(@NotNull sdlParserParser.VariableInitializerContext ctx);

	/**
	 * Enter a parse tree produced by {@link sdlParserParser#eb}.
	 * @param ctx the parse tree
	 */
	void enterEb(@NotNull sdlParserParser.EbContext ctx);
	/**
	 * Exit a parse tree produced by {@link sdlParserParser#eb}.
	 * @param ctx the parse tree
	 */
	void exitEb(@NotNull sdlParserParser.EbContext ctx);

	/**
	 * Enter a parse tree produced by {@link sdlParserParser#constantDeclaratorsRest}.
	 * @param ctx the parse tree
	 */
	void enterConstantDeclaratorsRest(@NotNull sdlParserParser.ConstantDeclaratorsRestContext ctx);
	/**
	 * Exit a parse tree produced by {@link sdlParserParser#constantDeclaratorsRest}.
	 * @param ctx the parse tree
	 */
	void exitConstantDeclaratorsRest(@NotNull sdlParserParser.ConstantDeclaratorsRestContext ctx);

	/**
	 * Enter a parse tree produced by {@link sdlParserParser#localVariableDeclarationStatement}.
	 * @param ctx the parse tree
	 */
	void enterLocalVariableDeclarationStatement(@NotNull sdlParserParser.LocalVariableDeclarationStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link sdlParserParser#localVariableDeclarationStatement}.
	 * @param ctx the parse tree
	 */
	void exitLocalVariableDeclarationStatement(@NotNull sdlParserParser.LocalVariableDeclarationStatementContext ctx);

	/**
	 * Enter a parse tree produced by {@link sdlParserParser#interfaceMethodOrFieldDecl}.
	 * @param ctx the parse tree
	 */
	void enterInterfaceMethodOrFieldDecl(@NotNull sdlParserParser.InterfaceMethodOrFieldDeclContext ctx);
	/**
	 * Exit a parse tree produced by {@link sdlParserParser#interfaceMethodOrFieldDecl}.
	 * @param ctx the parse tree
	 */
	void exitInterfaceMethodOrFieldDecl(@NotNull sdlParserParser.InterfaceMethodOrFieldDeclContext ctx);

	/**
	 * Enter a parse tree produced by {@link sdlParserParser#fieldDeclaration}.
	 * @param ctx the parse tree
	 */
	void enterFieldDeclaration(@NotNull sdlParserParser.FieldDeclarationContext ctx);
	/**
	 * Exit a parse tree produced by {@link sdlParserParser#fieldDeclaration}.
	 * @param ctx the parse tree
	 */
	void exitFieldDeclaration(@NotNull sdlParserParser.FieldDeclarationContext ctx);

	/**
	 * Enter a parse tree produced by {@link sdlParserParser#normalInterfaceDeclaration}.
	 * @param ctx the parse tree
	 */
	void enterNormalInterfaceDeclaration(@NotNull sdlParserParser.NormalInterfaceDeclarationContext ctx);
	/**
	 * Exit a parse tree produced by {@link sdlParserParser#normalInterfaceDeclaration}.
	 * @param ctx the parse tree
	 */
	void exitNormalInterfaceDeclaration(@NotNull sdlParserParser.NormalInterfaceDeclarationContext ctx);

	/**
	 * Enter a parse tree produced by {@link sdlParserParser#modifiers}.
	 * @param ctx the parse tree
	 */
	void enterModifiers(@NotNull sdlParserParser.ModifiersContext ctx);
	/**
	 * Exit a parse tree produced by {@link sdlParserParser#modifiers}.
	 * @param ctx the parse tree
	 */
	void exitModifiers(@NotNull sdlParserParser.ModifiersContext ctx);

	/**
	 * Enter a parse tree produced by {@link sdlParserParser#explicitGenericInvocation}.
	 * @param ctx the parse tree
	 */
	void enterExplicitGenericInvocation(@NotNull sdlParserParser.ExplicitGenericInvocationContext ctx);
	/**
	 * Exit a parse tree produced by {@link sdlParserParser#explicitGenericInvocation}.
	 * @param ctx the parse tree
	 */
	void exitExplicitGenericInvocation(@NotNull sdlParserParser.ExplicitGenericInvocationContext ctx);

	/**
	 * Enter a parse tree produced by {@link sdlParserParser#member}.
	 * @param ctx the parse tree
	 */
	void enterMember(@NotNull sdlParserParser.MemberContext ctx);
	/**
	 * Exit a parse tree produced by {@link sdlParserParser#member}.
	 * @param ctx the parse tree
	 */
	void exitMember(@NotNull sdlParserParser.MemberContext ctx);

	/**
	 * Enter a parse tree produced by {@link sdlParserParser#parExpression}.
	 * @param ctx the parse tree
	 */
	void enterParExpression(@NotNull sdlParserParser.ParExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link sdlParserParser#parExpression}.
	 * @param ctx the parse tree
	 */
	void exitParExpression(@NotNull sdlParserParser.ParExpressionContext ctx);

	/**
	 * Enter a parse tree produced by {@link sdlParserParser#catches}.
	 * @param ctx the parse tree
	 */
	void enterCatches(@NotNull sdlParserParser.CatchesContext ctx);
	/**
	 * Exit a parse tree produced by {@link sdlParserParser#catches}.
	 * @param ctx the parse tree
	 */
	void exitCatches(@NotNull sdlParserParser.CatchesContext ctx);

	/**
	 * Enter a parse tree produced by {@link sdlParserParser#switchLabel}.
	 * @param ctx the parse tree
	 */
	void enterSwitchLabel(@NotNull sdlParserParser.SwitchLabelContext ctx);
	/**
	 * Exit a parse tree produced by {@link sdlParserParser#switchLabel}.
	 * @param ctx the parse tree
	 */
	void exitSwitchLabel(@NotNull sdlParserParser.SwitchLabelContext ctx);

	/**
	 * Enter a parse tree produced by {@link sdlParserParser#e}.
	 * @param ctx the parse tree
	 */
	void enterE(@NotNull sdlParserParser.EContext ctx);
	/**
	 * Exit a parse tree produced by {@link sdlParserParser#e}.
	 * @param ctx the parse tree
	 */
	void exitE(@NotNull sdlParserParser.EContext ctx);

	/**
	 * Enter a parse tree produced by {@link sdlParserParser#typeParameters}.
	 * @param ctx the parse tree
	 */
	void enterTypeParameters(@NotNull sdlParserParser.TypeParametersContext ctx);
	/**
	 * Exit a parse tree produced by {@link sdlParserParser#typeParameters}.
	 * @param ctx the parse tree
	 */
	void exitTypeParameters(@NotNull sdlParserParser.TypeParametersContext ctx);

	/**
	 * Enter a parse tree produced by {@link sdlParserParser#c}.
	 * @param ctx the parse tree
	 */
	void enterC(@NotNull sdlParserParser.CContext ctx);
	/**
	 * Exit a parse tree produced by {@link sdlParserParser#c}.
	 * @param ctx the parse tree
	 */
	void exitC(@NotNull sdlParserParser.CContext ctx);

	/**
	 * Enter a parse tree produced by {@link sdlParserParser#qualifiedName}.
	 * @param ctx the parse tree
	 */
	void enterQualifiedName(@NotNull sdlParserParser.QualifiedNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link sdlParserParser#qualifiedName}.
	 * @param ctx the parse tree
	 */
	void exitQualifiedName(@NotNull sdlParserParser.QualifiedNameContext ctx);

	/**
	 * Enter a parse tree produced by {@link sdlParserParser#classDeclaration}.
	 * @param ctx the parse tree
	 */
	void enterClassDeclaration(@NotNull sdlParserParser.ClassDeclarationContext ctx);
	/**
	 * Exit a parse tree produced by {@link sdlParserParser#classDeclaration}.
	 * @param ctx the parse tree
	 */
	void exitClassDeclaration(@NotNull sdlParserParser.ClassDeclarationContext ctx);

	/**
	 * Enter a parse tree produced by {@link sdlParserParser#o}.
	 * @param ctx the parse tree
	 */
	void enterO(@NotNull sdlParserParser.OContext ctx);
	/**
	 * Exit a parse tree produced by {@link sdlParserParser#o}.
	 * @param ctx the parse tree
	 */
	void exitO(@NotNull sdlParserParser.OContext ctx);

	/**
	 * Enter a parse tree produced by {@link sdlParserParser#booleanLiteral}.
	 * @param ctx the parse tree
	 */
	void enterBooleanLiteral(@NotNull sdlParserParser.BooleanLiteralContext ctx);
	/**
	 * Exit a parse tree produced by {@link sdlParserParser#booleanLiteral}.
	 * @param ctx the parse tree
	 */
	void exitBooleanLiteral(@NotNull sdlParserParser.BooleanLiteralContext ctx);

	/**
	 * Enter a parse tree produced by {@link sdlParserParser#annotationConstantRest}.
	 * @param ctx the parse tree
	 */
	void enterAnnotationConstantRest(@NotNull sdlParserParser.AnnotationConstantRestContext ctx);
	/**
	 * Exit a parse tree produced by {@link sdlParserParser#annotationConstantRest}.
	 * @param ctx the parse tree
	 */
	void exitAnnotationConstantRest(@NotNull sdlParserParser.AnnotationConstantRestContext ctx);

	/**
	 * Enter a parse tree produced by {@link sdlParserParser#fd}.
	 * @param ctx the parse tree
	 */
	void enterFd(@NotNull sdlParserParser.FdContext ctx);
	/**
	 * Exit a parse tree produced by {@link sdlParserParser#fd}.
	 * @param ctx the parse tree
	 */
	void exitFd(@NotNull sdlParserParser.FdContext ctx);

	/**
	 * Enter a parse tree produced by {@link sdlParserParser#h}.
	 * @param ctx the parse tree
	 */
	void enterH(@NotNull sdlParserParser.HContext ctx);
	/**
	 * Exit a parse tree produced by {@link sdlParserParser#h}.
	 * @param ctx the parse tree
	 */
	void exitH(@NotNull sdlParserParser.HContext ctx);

	/**
	 * Enter a parse tree produced by {@link sdlParserParser#typeName}.
	 * @param ctx the parse tree
	 */
	void enterTypeName(@NotNull sdlParserParser.TypeNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link sdlParserParser#typeName}.
	 * @param ctx the parse tree
	 */
	void exitTypeName(@NotNull sdlParserParser.TypeNameContext ctx);

	/**
	 * Enter a parse tree produced by {@link sdlParserParser#arguments}.
	 * @param ctx the parse tree
	 */
	void enterArguments(@NotNull sdlParserParser.ArgumentsContext ctx);
	/**
	 * Exit a parse tree produced by {@link sdlParserParser#arguments}.
	 * @param ctx the parse tree
	 */
	void exitArguments(@NotNull sdlParserParser.ArgumentsContext ctx);

	/**
	 * Enter a parse tree produced by {@link sdlParserParser#constructorBody}.
	 * @param ctx the parse tree
	 */
	void enterConstructorBody(@NotNull sdlParserParser.ConstructorBodyContext ctx);
	/**
	 * Exit a parse tree produced by {@link sdlParserParser#constructorBody}.
	 * @param ctx the parse tree
	 */
	void exitConstructorBody(@NotNull sdlParserParser.ConstructorBodyContext ctx);

	/**
	 * Enter a parse tree produced by {@link sdlParserParser#TSP}.
	 * @param ctx the parse tree
	 */
	void enterTSP(@NotNull sdlParserParser.TSPContext ctx);
	/**
	 * Exit a parse tree produced by {@link sdlParserParser#TSP}.
	 * @param ctx the parse tree
	 */
	void exitTSP(@NotNull sdlParserParser.TSPContext ctx);

	/**
	 * Enter a parse tree produced by {@link sdlParserParser#s}.
	 * @param ctx the parse tree
	 */
	void enterS(@NotNull sdlParserParser.SContext ctx);
	/**
	 * Exit a parse tree produced by {@link sdlParserParser#s}.
	 * @param ctx the parse tree
	 */
	void exitS(@NotNull sdlParserParser.SContext ctx);

	/**
	 * Enter a parse tree produced by {@link sdlParserParser#ek}.
	 * @param ctx the parse tree
	 */
	void enterEk(@NotNull sdlParserParser.EkContext ctx);
	/**
	 * Exit a parse tree produced by {@link sdlParserParser#ek}.
	 * @param ctx the parse tree
	 */
	void exitEk(@NotNull sdlParserParser.EkContext ctx);

	/**
	 * Enter a parse tree produced by {@link sdlParserParser#formalParameters}.
	 * @param ctx the parse tree
	 */
	void enterFormalParameters(@NotNull sdlParserParser.FormalParametersContext ctx);
	/**
	 * Exit a parse tree produced by {@link sdlParserParser#formalParameters}.
	 * @param ctx the parse tree
	 */
	void exitFormalParameters(@NotNull sdlParserParser.FormalParametersContext ctx);

	/**
	 * Enter a parse tree produced by {@link sdlParserParser#p}.
	 * @param ctx the parse tree
	 */
	void enterP(@NotNull sdlParserParser.PContext ctx);
	/**
	 * Exit a parse tree produced by {@link sdlParserParser#p}.
	 * @param ctx the parse tree
	 */
	void exitP(@NotNull sdlParserParser.PContext ctx);

	/**
	 * Enter a parse tree produced by {@link sdlParserParser#typeArgument}.
	 * @param ctx the parse tree
	 */
	void enterTypeArgument(@NotNull sdlParserParser.TypeArgumentContext ctx);
	/**
	 * Exit a parse tree produced by {@link sdlParserParser#typeArgument}.
	 * @param ctx the parse tree
	 */
	void exitTypeArgument(@NotNull sdlParserParser.TypeArgumentContext ctx);

	/**
	 * Enter a parse tree produced by {@link sdlParserParser#forInit}.
	 * @param ctx the parse tree
	 */
	void enterForInit(@NotNull sdlParserParser.ForInitContext ctx);
	/**
	 * Exit a parse tree produced by {@link sdlParserParser#forInit}.
	 * @param ctx the parse tree
	 */
	void exitForInit(@NotNull sdlParserParser.ForInitContext ctx);

	/**
	 * Enter a parse tree produced by {@link sdlParserParser#annotations}.
	 * @param ctx the parse tree
	 */
	void enterAnnotations(@NotNull sdlParserParser.AnnotationsContext ctx);
	/**
	 * Exit a parse tree produced by {@link sdlParserParser#annotations}.
	 * @param ctx the parse tree
	 */
	void exitAnnotations(@NotNull sdlParserParser.AnnotationsContext ctx);

	/**
	 * Enter a parse tree produced by {@link sdlParserParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterExpression(@NotNull sdlParserParser.ExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link sdlParserParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitExpression(@NotNull sdlParserParser.ExpressionContext ctx);

	/**
	 * Enter a parse tree produced by {@link sdlParserParser#variableDeclarator}.
	 * @param ctx the parse tree
	 */
	void enterVariableDeclarator(@NotNull sdlParserParser.VariableDeclaratorContext ctx);
	/**
	 * Exit a parse tree produced by {@link sdlParserParser#variableDeclarator}.
	 * @param ctx the parse tree
	 */
	void exitVariableDeclarator(@NotNull sdlParserParser.VariableDeclaratorContext ctx);

	/**
	 * Enter a parse tree produced by {@link sdlParserParser#annotationTypeDeclaration}.
	 * @param ctx the parse tree
	 */
	void enterAnnotationTypeDeclaration(@NotNull sdlParserParser.AnnotationTypeDeclarationContext ctx);
	/**
	 * Exit a parse tree produced by {@link sdlParserParser#annotationTypeDeclaration}.
	 * @param ctx the parse tree
	 */
	void exitAnnotationTypeDeclaration(@NotNull sdlParserParser.AnnotationTypeDeclarationContext ctx);

	/**
	 * Enter a parse tree produced by {@link sdlParserParser#formalParameter}.
	 * @param ctx the parse tree
	 */
	void enterFormalParameter(@NotNull sdlParserParser.FormalParameterContext ctx);
	/**
	 * Exit a parse tree produced by {@link sdlParserParser#formalParameter}.
	 * @param ctx the parse tree
	 */
	void exitFormalParameter(@NotNull sdlParserParser.FormalParameterContext ctx);

	/**
	 * Enter a parse tree produced by {@link sdlParserParser#type}.
	 * @param ctx the parse tree
	 */
	void enterType(@NotNull sdlParserParser.TypeContext ctx);
	/**
	 * Exit a parse tree produced by {@link sdlParserParser#type}.
	 * @param ctx the parse tree
	 */
	void exitType(@NotNull sdlParserParser.TypeContext ctx);

	/**
	 * Enter a parse tree produced by {@link sdlParserParser#elementValueArrayInitializer}.
	 * @param ctx the parse tree
	 */
	void enterElementValueArrayInitializer(@NotNull sdlParserParser.ElementValueArrayInitializerContext ctx);
	/**
	 * Exit a parse tree produced by {@link sdlParserParser#elementValueArrayInitializer}.
	 * @param ctx the parse tree
	 */
	void exitElementValueArrayInitializer(@NotNull sdlParserParser.ElementValueArrayInitializerContext ctx);

	/**
	 * Enter a parse tree produced by {@link sdlParserParser#annotationName}.
	 * @param ctx the parse tree
	 */
	void enterAnnotationName(@NotNull sdlParserParser.AnnotationNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link sdlParserParser#annotationName}.
	 * @param ctx the parse tree
	 */
	void exitAnnotationName(@NotNull sdlParserParser.AnnotationNameContext ctx);

	/**
	 * Enter a parse tree produced by {@link sdlParserParser#tb}.
	 * @param ctx the parse tree
	 */
	void enterTb(@NotNull sdlParserParser.TbContext ctx);
	/**
	 * Exit a parse tree produced by {@link sdlParserParser#tb}.
	 * @param ctx the parse tree
	 */
	void exitTb(@NotNull sdlParserParser.TbContext ctx);

	/**
	 * Enter a parse tree produced by {@link sdlParserParser#enhancedForControl}.
	 * @param ctx the parse tree
	 */
	void enterEnhancedForControl(@NotNull sdlParserParser.EnhancedForControlContext ctx);
	/**
	 * Exit a parse tree produced by {@link sdlParserParser#enhancedForControl}.
	 * @param ctx the parse tree
	 */
	void exitEnhancedForControl(@NotNull sdlParserParser.EnhancedForControlContext ctx);

	/**
	 * Enter a parse tree produced by {@link sdlParserParser#annotationMethodRest}.
	 * @param ctx the parse tree
	 */
	void enterAnnotationMethodRest(@NotNull sdlParserParser.AnnotationMethodRestContext ctx);
	/**
	 * Exit a parse tree produced by {@link sdlParserParser#annotationMethodRest}.
	 * @param ctx the parse tree
	 */
	void exitAnnotationMethodRest(@NotNull sdlParserParser.AnnotationMethodRestContext ctx);

	/**
	 * Enter a parse tree produced by {@link sdlParserParser#formalParameterDeclsRest}.
	 * @param ctx the parse tree
	 */
	void enterFormalParameterDeclsRest(@NotNull sdlParserParser.FormalParameterDeclsRestContext ctx);
	/**
	 * Exit a parse tree produced by {@link sdlParserParser#formalParameterDeclsRest}.
	 * @param ctx the parse tree
	 */
	void exitFormalParameterDeclsRest(@NotNull sdlParserParser.FormalParameterDeclsRestContext ctx);

	/**
	 * Enter a parse tree produced by {@link sdlParserParser#primary}.
	 * @param ctx the parse tree
	 */
	void enterPrimary(@NotNull sdlParserParser.PrimaryContext ctx);
	/**
	 * Exit a parse tree produced by {@link sdlParserParser#primary}.
	 * @param ctx the parse tree
	 */
	void exitPrimary(@NotNull sdlParserParser.PrimaryContext ctx);

	/**
	 * Enter a parse tree produced by {@link sdlParserParser#classBody}.
	 * @param ctx the parse tree
	 */
	void enterClassBody(@NotNull sdlParserParser.ClassBodyContext ctx);
	/**
	 * Exit a parse tree produced by {@link sdlParserParser#classBody}.
	 * @param ctx the parse tree
	 */
	void exitClassBody(@NotNull sdlParserParser.ClassBodyContext ctx);

	/**
	 * Enter a parse tree produced by {@link sdlParserParser#defaultValue}.
	 * @param ctx the parse tree
	 */
	void enterDefaultValue(@NotNull sdlParserParser.DefaultValueContext ctx);
	/**
	 * Exit a parse tree produced by {@link sdlParserParser#defaultValue}.
	 * @param ctx the parse tree
	 */
	void exitDefaultValue(@NotNull sdlParserParser.DefaultValueContext ctx);

	/**
	 * Enter a parse tree produced by {@link sdlParserParser#classOrInterfaceModifier}.
	 * @param ctx the parse tree
	 */
	void enterClassOrInterfaceModifier(@NotNull sdlParserParser.ClassOrInterfaceModifierContext ctx);
	/**
	 * Exit a parse tree produced by {@link sdlParserParser#classOrInterfaceModifier}.
	 * @param ctx the parse tree
	 */
	void exitClassOrInterfaceModifier(@NotNull sdlParserParser.ClassOrInterfaceModifierContext ctx);

	/**
	 * Enter a parse tree produced by {@link sdlParserParser#TPS}.
	 * @param ctx the parse tree
	 */
	void enterTPS(@NotNull sdlParserParser.TPSContext ctx);
	/**
	 * Exit a parse tree produced by {@link sdlParserParser#TPS}.
	 * @param ctx the parse tree
	 */
	void exitTPS(@NotNull sdlParserParser.TPSContext ctx);

	/**
	 * Enter a parse tree produced by {@link sdlParserParser#variableModifier}.
	 * @param ctx the parse tree
	 */
	void enterVariableModifier(@NotNull sdlParserParser.VariableModifierContext ctx);
	/**
	 * Exit a parse tree produced by {@link sdlParserParser#variableModifier}.
	 * @param ctx the parse tree
	 */
	void exitVariableModifier(@NotNull sdlParserParser.VariableModifierContext ctx);

	/**
	 * Enter a parse tree produced by {@link sdlParserParser#createdName}.
	 * @param ctx the parse tree
	 */
	void enterCreatedName(@NotNull sdlParserParser.CreatedNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link sdlParserParser#createdName}.
	 * @param ctx the parse tree
	 */
	void exitCreatedName(@NotNull sdlParserParser.CreatedNameContext ctx);

	/**
	 * Enter a parse tree produced by {@link sdlParserParser#interfaceDeclaration}.
	 * @param ctx the parse tree
	 */
	void enterInterfaceDeclaration(@NotNull sdlParserParser.InterfaceDeclarationContext ctx);
	/**
	 * Exit a parse tree produced by {@link sdlParserParser#interfaceDeclaration}.
	 * @param ctx the parse tree
	 */
	void exitInterfaceDeclaration(@NotNull sdlParserParser.InterfaceDeclarationContext ctx);

	/**
	 * Enter a parse tree produced by {@link sdlParserParser#packageDeclaration}.
	 * @param ctx the parse tree
	 */
	void enterPackageDeclaration(@NotNull sdlParserParser.PackageDeclarationContext ctx);
	/**
	 * Exit a parse tree produced by {@link sdlParserParser#packageDeclaration}.
	 * @param ctx the parse tree
	 */
	void exitPackageDeclaration(@NotNull sdlParserParser.PackageDeclarationContext ctx);

	/**
	 * Enter a parse tree produced by {@link sdlParserParser#PST}.
	 * @param ctx the parse tree
	 */
	void enterPST(@NotNull sdlParserParser.PSTContext ctx);
	/**
	 * Exit a parse tree produced by {@link sdlParserParser#PST}.
	 * @param ctx the parse tree
	 */
	void exitPST(@NotNull sdlParserParser.PSTContext ctx);

	/**
	 * Enter a parse tree produced by {@link sdlParserParser#constantDeclarator}.
	 * @param ctx the parse tree
	 */
	void enterConstantDeclarator(@NotNull sdlParserParser.ConstantDeclaratorContext ctx);
	/**
	 * Exit a parse tree produced by {@link sdlParserParser#constantDeclarator}.
	 * @param ctx the parse tree
	 */
	void exitConstantDeclarator(@NotNull sdlParserParser.ConstantDeclaratorContext ctx);

	/**
	 * Enter a parse tree produced by {@link sdlParserParser#constantDeclaratorRest}.
	 * @param ctx the parse tree
	 */
	void enterConstantDeclaratorRest(@NotNull sdlParserParser.ConstantDeclaratorRestContext ctx);
	/**
	 * Exit a parse tree produced by {@link sdlParserParser#constantDeclaratorRest}.
	 * @param ctx the parse tree
	 */
	void exitConstantDeclaratorRest(@NotNull sdlParserParser.ConstantDeclaratorRestContext ctx);

	/**
	 * Enter a parse tree produced by {@link sdlParserParser#typeArguments}.
	 * @param ctx the parse tree
	 */
	void enterTypeArguments(@NotNull sdlParserParser.TypeArgumentsContext ctx);
	/**
	 * Exit a parse tree produced by {@link sdlParserParser#typeArguments}.
	 * @param ctx the parse tree
	 */
	void exitTypeArguments(@NotNull sdlParserParser.TypeArgumentsContext ctx);

	/**
	 * Enter a parse tree produced by {@link sdlParserParser#classCreatorRest}.
	 * @param ctx the parse tree
	 */
	void enterClassCreatorRest(@NotNull sdlParserParser.ClassCreatorRestContext ctx);
	/**
	 * Exit a parse tree produced by {@link sdlParserParser#classCreatorRest}.
	 * @param ctx the parse tree
	 */
	void exitClassCreatorRest(@NotNull sdlParserParser.ClassCreatorRestContext ctx);

	/**
	 * Enter a parse tree produced by {@link sdlParserParser#statement}.
	 * @param ctx the parse tree
	 */
	void enterStatement(@NotNull sdlParserParser.StatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link sdlParserParser#statement}.
	 * @param ctx the parse tree
	 */
	void exitStatement(@NotNull sdlParserParser.StatementContext ctx);

	/**
	 * Enter a parse tree produced by {@link sdlParserParser#modifier}.
	 * @param ctx the parse tree
	 */
	void enterModifier(@NotNull sdlParserParser.ModifierContext ctx);
	/**
	 * Exit a parse tree produced by {@link sdlParserParser#modifier}.
	 * @param ctx the parse tree
	 */
	void exitModifier(@NotNull sdlParserParser.ModifierContext ctx);

	/**
	 * Enter a parse tree produced by {@link sdlParserParser#interfaceBody}.
	 * @param ctx the parse tree
	 */
	void enterInterfaceBody(@NotNull sdlParserParser.InterfaceBodyContext ctx);
	/**
	 * Exit a parse tree produced by {@link sdlParserParser#interfaceBody}.
	 * @param ctx the parse tree
	 */
	void exitInterfaceBody(@NotNull sdlParserParser.InterfaceBodyContext ctx);

	/**
	 * Enter a parse tree produced by {@link sdlParserParser#classBodyDeclaration}.
	 * @param ctx the parse tree
	 */
	void enterClassBodyDeclaration(@NotNull sdlParserParser.ClassBodyDeclarationContext ctx);
	/**
	 * Exit a parse tree produced by {@link sdlParserParser#classBodyDeclaration}.
	 * @param ctx the parse tree
	 */
	void exitClassBodyDeclaration(@NotNull sdlParserParser.ClassBodyDeclarationContext ctx);

	/**
	 * Enter a parse tree produced by {@link sdlParserParser#packageOrTypeName}.
	 * @param ctx the parse tree
	 */
	void enterPackageOrTypeName(@NotNull sdlParserParser.PackageOrTypeNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link sdlParserParser#packageOrTypeName}.
	 * @param ctx the parse tree
	 */
	void exitPackageOrTypeName(@NotNull sdlParserParser.PackageOrTypeNameContext ctx);

	/**
	 * Enter a parse tree produced by {@link sdlParserParser#forControl}.
	 * @param ctx the parse tree
	 */
	void enterForControl(@NotNull sdlParserParser.ForControlContext ctx);
	/**
	 * Exit a parse tree produced by {@link sdlParserParser#forControl}.
	 * @param ctx the parse tree
	 */
	void exitForControl(@NotNull sdlParserParser.ForControlContext ctx);

	/**
	 * Enter a parse tree produced by {@link sdlParserParser#localVariableDeclaration}.
	 * @param ctx the parse tree
	 */
	void enterLocalVariableDeclaration(@NotNull sdlParserParser.LocalVariableDeclarationContext ctx);
	/**
	 * Exit a parse tree produced by {@link sdlParserParser#localVariableDeclaration}.
	 * @param ctx the parse tree
	 */
	void exitLocalVariableDeclaration(@NotNull sdlParserParser.LocalVariableDeclarationContext ctx);

	/**
	 * Enter a parse tree produced by {@link sdlParserParser#typeList}.
	 * @param ctx the parse tree
	 */
	void enterTypeList(@NotNull sdlParserParser.TypeListContext ctx);
	/**
	 * Exit a parse tree produced by {@link sdlParserParser#typeList}.
	 * @param ctx the parse tree
	 */
	void exitTypeList(@NotNull sdlParserParser.TypeListContext ctx);

	/**
	 * Enter a parse tree produced by {@link sdlParserParser#STP}.
	 * @param ctx the parse tree
	 */
	void enterSTP(@NotNull sdlParserParser.STPContext ctx);
	/**
	 * Exit a parse tree produced by {@link sdlParserParser#STP}.
	 * @param ctx the parse tree
	 */
	void exitSTP(@NotNull sdlParserParser.STPContext ctx);

	/**
	 * Enter a parse tree produced by {@link sdlParserParser#variableDeclaratorId}.
	 * @param ctx the parse tree
	 */
	void enterVariableDeclaratorId(@NotNull sdlParserParser.VariableDeclaratorIdContext ctx);
	/**
	 * Exit a parse tree produced by {@link sdlParserParser#variableDeclaratorId}.
	 * @param ctx the parse tree
	 */
	void exitVariableDeclaratorId(@NotNull sdlParserParser.VariableDeclaratorIdContext ctx);

	/**
	 * Enter a parse tree produced by {@link sdlParserParser#compilationUnit}.
	 * @param ctx the parse tree
	 */
	void enterCompilationUnit(@NotNull sdlParserParser.CompilationUnitContext ctx);
	/**
	 * Exit a parse tree produced by {@link sdlParserParser#compilationUnit}.
	 * @param ctx the parse tree
	 */
	void exitCompilationUnit(@NotNull sdlParserParser.CompilationUnitContext ctx);

	/**
	 * Enter a parse tree produced by {@link sdlParserParser#elementValue}.
	 * @param ctx the parse tree
	 */
	void enterElementValue(@NotNull sdlParserParser.ElementValueContext ctx);
	/**
	 * Exit a parse tree produced by {@link sdlParserParser#elementValue}.
	 * @param ctx the parse tree
	 */
	void exitElementValue(@NotNull sdlParserParser.ElementValueContext ctx);

	/**
	 * Enter a parse tree produced by {@link sdlParserParser#classOrInterfaceType}.
	 * @param ctx the parse tree
	 */
	void enterClassOrInterfaceType(@NotNull sdlParserParser.ClassOrInterfaceTypeContext ctx);
	/**
	 * Exit a parse tree produced by {@link sdlParserParser#classOrInterfaceType}.
	 * @param ctx the parse tree
	 */
	void exitClassOrInterfaceType(@NotNull sdlParserParser.ClassOrInterfaceTypeContext ctx);

	/**
	 * Enter a parse tree produced by {@link sdlParserParser#blockStatement}.
	 * @param ctx the parse tree
	 */
	void enterBlockStatement(@NotNull sdlParserParser.BlockStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link sdlParserParser#blockStatement}.
	 * @param ctx the parse tree
	 */
	void exitBlockStatement(@NotNull sdlParserParser.BlockStatementContext ctx);

	/**
	 * Enter a parse tree produced by {@link sdlParserParser#annotationTypeElementDeclaration}.
	 * @param ctx the parse tree
	 */
	void enterAnnotationTypeElementDeclaration(@NotNull sdlParserParser.AnnotationTypeElementDeclarationContext ctx);
	/**
	 * Exit a parse tree produced by {@link sdlParserParser#annotationTypeElementDeclaration}.
	 * @param ctx the parse tree
	 */
	void exitAnnotationTypeElementDeclaration(@NotNull sdlParserParser.AnnotationTypeElementDeclarationContext ctx);

	/**
	 * Enter a parse tree produced by {@link sdlParserParser#annotationTypeBody}.
	 * @param ctx the parse tree
	 */
	void enterAnnotationTypeBody(@NotNull sdlParserParser.AnnotationTypeBodyContext ctx);
	/**
	 * Exit a parse tree produced by {@link sdlParserParser#annotationTypeBody}.
	 * @param ctx the parse tree
	 */
	void exitAnnotationTypeBody(@NotNull sdlParserParser.AnnotationTypeBodyContext ctx);

	/**
	 * Enter a parse tree produced by {@link sdlParserParser#interfaceMemberDecl}.
	 * @param ctx the parse tree
	 */
	void enterInterfaceMemberDecl(@NotNull sdlParserParser.InterfaceMemberDeclContext ctx);
	/**
	 * Exit a parse tree produced by {@link sdlParserParser#interfaceMemberDecl}.
	 * @param ctx the parse tree
	 */
	void exitInterfaceMemberDecl(@NotNull sdlParserParser.InterfaceMemberDeclContext ctx);

	/**
	 * Enter a parse tree produced by {@link sdlParserParser#integerLiteral}.
	 * @param ctx the parse tree
	 */
	void enterIntegerLiteral(@NotNull sdlParserParser.IntegerLiteralContext ctx);
	/**
	 * Exit a parse tree produced by {@link sdlParserParser#integerLiteral}.
	 * @param ctx the parse tree
	 */
	void exitIntegerLiteral(@NotNull sdlParserParser.IntegerLiteralContext ctx);

	/**
	 * Enter a parse tree produced by {@link sdlParserParser#creator}.
	 * @param ctx the parse tree
	 */
	void enterCreator(@NotNull sdlParserParser.CreatorContext ctx);
	/**
	 * Exit a parse tree produced by {@link sdlParserParser#creator}.
	 * @param ctx the parse tree
	 */
	void exitCreator(@NotNull sdlParserParser.CreatorContext ctx);

	/**
	 * Enter a parse tree produced by {@link sdlParserParser#qualifiedNameList}.
	 * @param ctx the parse tree
	 */
	void enterQualifiedNameList(@NotNull sdlParserParser.QualifiedNameListContext ctx);
	/**
	 * Exit a parse tree produced by {@link sdlParserParser#qualifiedNameList}.
	 * @param ctx the parse tree
	 */
	void exitQualifiedNameList(@NotNull sdlParserParser.QualifiedNameListContext ctx);

	/**
	 * Enter a parse tree produced by {@link sdlParserParser#interfaceGenericMethodDecl}.
	 * @param ctx the parse tree
	 */
	void enterInterfaceGenericMethodDecl(@NotNull sdlParserParser.InterfaceGenericMethodDeclContext ctx);
	/**
	 * Exit a parse tree produced by {@link sdlParserParser#interfaceGenericMethodDecl}.
	 * @param ctx the parse tree
	 */
	void exitInterfaceGenericMethodDecl(@NotNull sdlParserParser.InterfaceGenericMethodDeclContext ctx);

	/**
	 * Enter a parse tree produced by {@link sdlParserParser#formalParameterDecls}.
	 * @param ctx the parse tree
	 */
	void enterFormalParameterDecls(@NotNull sdlParserParser.FormalParameterDeclsContext ctx);
	/**
	 * Exit a parse tree produced by {@link sdlParserParser#formalParameterDecls}.
	 * @param ctx the parse tree
	 */
	void exitFormalParameterDecls(@NotNull sdlParserParser.FormalParameterDeclsContext ctx);

	/**
	 * Enter a parse tree produced by {@link sdlParserParser#voidMethodDeclaratorRest}.
	 * @param ctx the parse tree
	 */
	void enterVoidMethodDeclaratorRest(@NotNull sdlParserParser.VoidMethodDeclaratorRestContext ctx);
	/**
	 * Exit a parse tree produced by {@link sdlParserParser#voidMethodDeclaratorRest}.
	 * @param ctx the parse tree
	 */
	void exitVoidMethodDeclaratorRest(@NotNull sdlParserParser.VoidMethodDeclaratorRestContext ctx);

	/**
	 * Enter a parse tree produced by {@link sdlParserParser#PTS}.
	 * @param ctx the parse tree
	 */
	void enterPTS(@NotNull sdlParserParser.PTSContext ctx);
	/**
	 * Exit a parse tree produced by {@link sdlParserParser#PTS}.
	 * @param ctx the parse tree
	 */
	void exitPTS(@NotNull sdlParserParser.PTSContext ctx);

	/**
	 * Enter a parse tree produced by {@link sdlParserParser#methodDeclaration}.
	 * @param ctx the parse tree
	 */
	void enterMethodDeclaration(@NotNull sdlParserParser.MethodDeclarationContext ctx);
	/**
	 * Exit a parse tree produced by {@link sdlParserParser#methodDeclaration}.
	 * @param ctx the parse tree
	 */
	void exitMethodDeclaration(@NotNull sdlParserParser.MethodDeclarationContext ctx);

	/**
	 * Enter a parse tree produced by {@link sdlParserParser#annotationTypeElementRest}.
	 * @param ctx the parse tree
	 */
	void enterAnnotationTypeElementRest(@NotNull sdlParserParser.AnnotationTypeElementRestContext ctx);
	/**
	 * Exit a parse tree produced by {@link sdlParserParser#annotationTypeElementRest}.
	 * @param ctx the parse tree
	 */
	void exitAnnotationTypeElementRest(@NotNull sdlParserParser.AnnotationTypeElementRestContext ctx);

	/**
	 * Enter a parse tree produced by {@link sdlParserParser#methodDeclarationRest}.
	 * @param ctx the parse tree
	 */
	void enterMethodDeclarationRest(@NotNull sdlParserParser.MethodDeclarationRestContext ctx);
	/**
	 * Exit a parse tree produced by {@link sdlParserParser#methodDeclarationRest}.
	 * @param ctx the parse tree
	 */
	void exitMethodDeclarationRest(@NotNull sdlParserParser.MethodDeclarationRestContext ctx);

	/**
	 * Enter a parse tree produced by {@link sdlParserParser#constructorDeclaration}.
	 * @param ctx the parse tree
	 */
	void enterConstructorDeclaration(@NotNull sdlParserParser.ConstructorDeclarationContext ctx);
	/**
	 * Exit a parse tree produced by {@link sdlParserParser#constructorDeclaration}.
	 * @param ctx the parse tree
	 */
	void exitConstructorDeclaration(@NotNull sdlParserParser.ConstructorDeclarationContext ctx);

	/**
	 * Enter a parse tree produced by {@link sdlParserParser#voidInterfaceMethodDeclaratorRest}.
	 * @param ctx the parse tree
	 */
	void enterVoidInterfaceMethodDeclaratorRest(@NotNull sdlParserParser.VoidInterfaceMethodDeclaratorRestContext ctx);
	/**
	 * Exit a parse tree produced by {@link sdlParserParser#voidInterfaceMethodDeclaratorRest}.
	 * @param ctx the parse tree
	 */
	void exitVoidInterfaceMethodDeclaratorRest(@NotNull sdlParserParser.VoidInterfaceMethodDeclaratorRestContext ctx);

	/**
	 * Enter a parse tree produced by {@link sdlParserParser#elementValuePair}.
	 * @param ctx the parse tree
	 */
	void enterElementValuePair(@NotNull sdlParserParser.ElementValuePairContext ctx);
	/**
	 * Exit a parse tree produced by {@link sdlParserParser#elementValuePair}.
	 * @param ctx the parse tree
	 */
	void exitElementValuePair(@NotNull sdlParserParser.ElementValuePairContext ctx);

	/**
	 * Enter a parse tree produced by {@link sdlParserParser#interfaceMethodOrFieldRest}.
	 * @param ctx the parse tree
	 */
	void enterInterfaceMethodOrFieldRest(@NotNull sdlParserParser.InterfaceMethodOrFieldRestContext ctx);
	/**
	 * Exit a parse tree produced by {@link sdlParserParser#interfaceMethodOrFieldRest}.
	 * @param ctx the parse tree
	 */
	void exitInterfaceMethodOrFieldRest(@NotNull sdlParserParser.InterfaceMethodOrFieldRestContext ctx);

	/**
	 * Enter a parse tree produced by {@link sdlParserParser#methodBody}.
	 * @param ctx the parse tree
	 */
	void enterMethodBody(@NotNull sdlParserParser.MethodBodyContext ctx);
	/**
	 * Exit a parse tree produced by {@link sdlParserParser#methodBody}.
	 * @param ctx the parse tree
	 */
	void exitMethodBody(@NotNull sdlParserParser.MethodBodyContext ctx);

	/**
	 * Enter a parse tree produced by {@link sdlParserParser#arrayInitializer}.
	 * @param ctx the parse tree
	 */
	void enterArrayInitializer(@NotNull sdlParserParser.ArrayInitializerContext ctx);
	/**
	 * Exit a parse tree produced by {@link sdlParserParser#arrayInitializer}.
	 * @param ctx the parse tree
	 */
	void exitArrayInitializer(@NotNull sdlParserParser.ArrayInitializerContext ctx);

	/**
	 * Enter a parse tree produced by {@link sdlParserParser#variableModifiers}.
	 * @param ctx the parse tree
	 */
	void enterVariableModifiers(@NotNull sdlParserParser.VariableModifiersContext ctx);
	/**
	 * Exit a parse tree produced by {@link sdlParserParser#variableModifiers}.
	 * @param ctx the parse tree
	 */
	void exitVariableModifiers(@NotNull sdlParserParser.VariableModifiersContext ctx);

	/**
	 * Enter a parse tree produced by {@link sdlParserParser#primitiveType}.
	 * @param ctx the parse tree
	 */
	void enterPrimitiveType(@NotNull sdlParserParser.PrimitiveTypeContext ctx);
	/**
	 * Exit a parse tree produced by {@link sdlParserParser#primitiveType}.
	 * @param ctx the parse tree
	 */
	void exitPrimitiveType(@NotNull sdlParserParser.PrimitiveTypeContext ctx);

	/**
	 * Enter a parse tree produced by {@link sdlParserParser#nonWildcardTypeArguments}.
	 * @param ctx the parse tree
	 */
	void enterNonWildcardTypeArguments(@NotNull sdlParserParser.NonWildcardTypeArgumentsContext ctx);
	/**
	 * Exit a parse tree produced by {@link sdlParserParser#nonWildcardTypeArguments}.
	 * @param ctx the parse tree
	 */
	void exitNonWildcardTypeArguments(@NotNull sdlParserParser.NonWildcardTypeArgumentsContext ctx);

	/**
	 * Enter a parse tree produced by {@link sdlParserParser#arrayCreatorRest}.
	 * @param ctx the parse tree
	 */
	void enterArrayCreatorRest(@NotNull sdlParserParser.ArrayCreatorRestContext ctx);
	/**
	 * Exit a parse tree produced by {@link sdlParserParser#arrayCreatorRest}.
	 * @param ctx the parse tree
	 */
	void exitArrayCreatorRest(@NotNull sdlParserParser.ArrayCreatorRestContext ctx);

	/**
	 * Enter a parse tree produced by {@link sdlParserParser#switchBlockStatementGroup}.
	 * @param ctx the parse tree
	 */
	void enterSwitchBlockStatementGroup(@NotNull sdlParserParser.SwitchBlockStatementGroupContext ctx);
	/**
	 * Exit a parse tree produced by {@link sdlParserParser#switchBlockStatementGroup}.
	 * @param ctx the parse tree
	 */
	void exitSwitchBlockStatementGroup(@NotNull sdlParserParser.SwitchBlockStatementGroupContext ctx);

	/**
	 * Enter a parse tree produced by {@link sdlParserParser#literal}.
	 * @param ctx the parse tree
	 */
	void enterLiteral(@NotNull sdlParserParser.LiteralContext ctx);
	/**
	 * Exit a parse tree produced by {@link sdlParserParser#literal}.
	 * @param ctx the parse tree
	 */
	void exitLiteral(@NotNull sdlParserParser.LiteralContext ctx);
}