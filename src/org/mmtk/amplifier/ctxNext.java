package org.mmtk.amplifier;
public class ctxNext {
	String ctxName;
	String ctxLink;
	int flag;

	public int getFlag() {
		return flag;
	}

	public void setFlag(int flag) {
		this.flag = flag;
	}

	public ctxNext(String n, String l, int f) {
		this.ctxName = n;
		this.ctxLink = l;
		this.flag = f;
	}

	public String getCtxName() {
		return ctxName;
	}

	public void setCtxName(String ctxName) {
		this.ctxName = ctxName;
	}

	public String getCtxLink() {
		return ctxLink;
	}

	public void setCtxLink(String ctxLink) {
		this.ctxLink = ctxLink;
	}
}
