package org.mmtk.amplifier;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeWalker;

public class SDLtoJAVA {

	//TODO use the second parameter to indicate the output path
	public static String FILE_SEPERATOR = System.getProperty("file.separator");
	public static String PACKAGE_PREFIX_NAME = "org.jikesrvm.perfblower";
	static {
		if (FILE_SEPERATOR.equals("\\")){
			PACKAGE_PREFIX_NAME = PACKAGE_PREFIX_NAME.replaceAll("\\.", "\\\\");
		}else{
			PACKAGE_PREFIX_NAME = PACKAGE_PREFIX_NAME.replaceAll("\\.", FILE_SEPERATOR);
		}
	}
	public static String OUTPUT_DIR_NAME = System.getProperty("user.dir")
										+ FILE_SEPERATOR + "TestResults"
										+ FILE_SEPERATOR + PACKAGE_PREFIX_NAME;
	
	
	/**
	 * Error Codes
	 */
	public static final int INPUT_FILE_ERROR = -1;
	public static final int OUTPUT_DIR_ERROR = -11;
	
	/**
	 * Error Message Printer
	 */
	public static void printErrorMessage(int errorType){
		System.err.print("[ERROR]: ");
		if (errorType == INPUT_FILE_ERROR){
			System.err.println("Open input file!");
		}else if (errorType == OUTPUT_DIR_ERROR){
			System.err.println("Output dir error!");
		}
	}
	
	@SuppressWarnings("unchecked")
	public static void main(String[] args) 
			throws Exception 
	{
		String inputFile = null;

		File dir = new File(".");
		if (args.length > 0)
			inputFile = args[0];

		if (args.length > 0){
			inputFile = args[0];
		}else{
			System.out.println("No file specified in command line, please input it in console:");
		}
		
		if (args.length > 1) 
			OUTPUT_DIR_NAME = args[1] + FILE_SEPERATOR + "TestResults"
					+ FILE_SEPERATOR + PACKAGE_PREFIX_NAME;; 

		InputStream is = System.in;
		if (inputFile != null) {
			try {
				is = new FileInputStream(inputFile);
			} catch (FileNotFoundException e) {
				printErrorMessage(INPUT_FILE_ERROR);
				System.exit(INPUT_FILE_ERROR);
			}
		}
		
		ANTLRInputStream input = new ANTLRInputStream(is);
		sdlParserLexer lexer = new sdlParserLexer(input);
		CommonTokenStream tokens = new CommonTokenStream(lexer);
		sdlParserParser parser = new sdlParserParser(tokens);
		ParseTree tree = parser.s(); // parse

		ParseTreeWalker walker = new ParseTreeWalker(); // create standard
		PrintStatementsListener extractor = new PrintStatementsListener(parser); // walker
		walker.walk(extractor, tree); // initiate walk of tree with listener

		ContextVisitor ctxVisitor = new ContextVisitor();
		ctxVisitor.visit(tree);

		// part 1 : create the Event.java
		// Map that defines the function signatures		
		Map<String, String> eventSignatures = new HashMap<String, String>();
		eventSignatures.put("on_read", "(Object o,int f,int w1,int w2)");
		eventSignatures.put("on_write", "(Object o,int f,int w1,int w2)");
		eventSignatures.put("on_call", "(Object o,int m,int w1,int w2)");
		eventSignatures.put("on_reached", "(Object o)");
		eventSignatures.put("on_reachedOnce", "(Object o)");
		eventSignatures.put("on_alloc", "(Object o)");
		
		// 1. Class definition
		StringBuilder buf = new StringBuilder();
		buf.append("package org.jikesrvm.perfblower;");
		buf.append("\n");
		buf.append("import java.util.HashMap;");
		buf.append("\n");
		buf.append("import java.util.Map;");
		buf.append("\n");
		buf.append("import static org.jikesrvm.objectmodel.MiscHeader.*;");
		buf.append("\n");
		buf.append("import org.jikesrvm.objectmodel.Amplifier;");
		buf.append("\n");
		buf.append("import org.jikesrvm.objectmodel.Deamplifier;");
		buf.append("\n");
		buf.append("import org.jikesrvm.objectmodel.Field;");
		buf.append("\n");
		buf.append("import org.jikesrvm.objectmodel.Word;");
		buf.append("\n");
		buf.append("public class Event{\n\n");

		// 2. Variable definition
		// make the list of objects that contain instNames ordered by data type
		Map<String, ArrayList<Entry<String, String>>> tObjsOrderedInsts = new HashMap<String, ArrayList<Entry<String, String>>>();

		Set<String> kList = extractor.tObjectDefs.keySet();
		for (String k : kList) {
			// load TObject
			TObjectDefinition tObj = extractor.tObjectDefs.get(k);
			int offset = 0;

			// update the offsets for each object
			for (Instance i : tObj.getInstances()) {
				i.offset = offset;
				offset += Tools.getTypeValue(i.type);
			}
		}

		// 3. Amplify and deamplify definition
		buf.append("public static void amplify(Object o){\n");
		buf.append("Amplifier.amplify(o);\n");
		buf.append("}\n");
		buf.append("\n");
		buf.append("public static void deamplify(Object o){\n");
		buf.append("Deamplifier.deamplify(o);\n");
		buf.append("}\n");
		buf.append("\n");
		
		// create the map of available functions
		StringBuilder funcBuf = new StringBuilder();
		funcBuf.append("private static final Map<String, Boolean> functionMap;\n");
		funcBuf.append("static\n{\n");
		funcBuf.append("\tfunctionMap = new HashMap<String, Boolean>();\n");

		// 4. Events definition		
		Iterator event_ctx = eventSignatures.entrySet().iterator();
		while (event_ctx.hasNext()) {
			Map.Entry entry = (Map.Entry) event_ctx.next();
			String name = entry.getKey().toString();
			String parameters = eventSignatures.get(name);
			buf.append("public static void ");
			buf.append(name + parameters);
			buf.append("{\n");
			
			String hashStatements = extractor.evStmts.get(name + parameters);
			if (hashStatements != null) {
				// update the statement definitions
				RegTransStmt rts = new RegTransStmt(extractor.tObjectDefs, 
						hashStatements.toString().trim());
				buf.append(rts.transStmt());
				
				funcBuf.append(String.format("\tfunctionMap.put(\"%s\", %s);\n", name, "true"));
			} else {
				funcBuf.append(String.format("\tfunctionMap.put(\"%s\", %s);\n", name, "false"));
			}
			
			buf.append("\n}\n");
		}
		
		// Add static get function for testing if function exists
		buf.append("public static boolean doesFunctionExist(String func) {\n");
		buf.append("\treturn functionMap.get(func);\n}\n");
		
		funcBuf.append("}\n");
		buf.append(funcBuf.toString());

		// 5. Context definition
		Iterator it_ctx = ctxVisitor.contextDefs.entrySet().iterator();
		// for print
		Map<String, String> printcxtDefs = new HashMap<String, String>();
		ArrayList<ctxNext> ctxNextList = new ArrayList<ctxNext>();
		ArrayList<String> orderedCtx = new ArrayList<String>();

		while (it_ctx.hasNext()) {
			Map.Entry entry = (Map.Entry) it_ctx.next();
			String ctxName = entry.getKey().toString();
			String[] seqTypePath = entry.getValue().toString().split("\\|");
			String seq = seqTypePath[0];
			String type = seqTypePath[1];
			String path = seqTypePath[2];
			String printctxDef = "new Context(\"" + ctxName + "\", " + seq
					+ ", " + type + ", " + path + ", ";

			if (ctxName.equals(extractor.rtCtx))
				printctxDef = printctxDef + "true)";
			else
				printctxDef = printctxDef + "false)";
			printcxtDefs.put(ctxName, printctxDef);

			ctxNextList.add(new ctxNext(ctxName, path, 0));
		}

		// find the first one
		int i, j;
		for (i = 0; i < ctxNextList.size(); i++) {
			if (ctxNextList.get(i).ctxLink.equals("null")) {
				orderedCtx.add(ctxNextList.get(i).ctxName);
				ctxNextList.get(i).setFlag(1);
			}
		}
		// order the context definition
		while (!Tools.allFlagSetted(ctxNextList)) {
			for (i = 0; i < orderedCtx.size(); i++) {
				for (j = 0; j < ctxNextList.size(); j++)
					if ((orderedCtx.get(i).equals(ctxNextList.get(j).ctxLink))
							&& (ctxNextList.get(j).flag != 1)) {
						orderedCtx.add(ctxNextList.get(j).ctxName);
						ctxNextList.get(j).setFlag(1);
					}
			}
		}
		
		// wrap the rest of event
		buf.append("}");

		// 6. Create the java file
		File outputDir = new File(OUTPUT_DIR_NAME);
		if (!outputDir.exists()){
			outputDir.mkdirs();
		}
		if (!outputDir.exists() || !outputDir.isDirectory()){
			printErrorMessage(OUTPUT_DIR_ERROR);
			System.exit(OUTPUT_DIR_ERROR);
		}
		String outputFileName = outputDir.getAbsolutePath() + FILE_SEPERATOR + "Event.java";
		
		File outputFile = new File(outputFileName);
		if (!outputFile.exists()) {
			outputFile.createNewFile();
		}
		
		PrintStream printStream = new PrintStream(
				new FileOutputStream(outputFileName));

		printStream.println(buf.toString());
		printStream.close();

		// ////////////////////////////////////////////////////////////////////////////////////////////////////
		// part 2 : create class for HISTORY
		String[] typeSizeArray = extractor.historyDef.replace("\"", "").split(
				"\\|");
		String className = extractor.historyName;
		String type = typeSizeArray[0];
		String size = typeSizeArray[1];
		
		String kind = extractor.partitionDefs.get(className);

		BufferedReader bufReader = new BufferedReader(new InputStreamReader(
				new FileInputStream(new File("HistoryTemplate.txt"))));

		StringBuffer strBuf = new StringBuffer();
		for (String tmp = null; (tmp = bufReader.readLine()) != null; tmp = null) {
			tmp = tmp.replaceAll("TEMP_CLASS", className);
			tmp = tmp.replaceAll("TEMP_KIND", kind);
			tmp = tmp.replaceAll("TEMP_TYPE", type);
			tmp = tmp.replaceAll("TEMP_SIZE", size);
			strBuf.append(tmp);
			strBuf.append(System.getProperty("line.separator"));
		}
		bufReader.close();

		outputFileName = outputDir.getAbsolutePath() + FILE_SEPERATOR  + className + ".java";
		outputFile = new File(outputFileName);
		if (!outputFile.exists()) {
			outputFile.createNewFile();
		}

		PrintStream printStream2 = new PrintStream(new FileOutputStream(
				outputFileName));
		printStream2.println(strBuf.toString());
		printStream2.close();

		// ////////////////////////////////////////////////////////////////////////////////////////////////////
		// part 3 : create class for CONTEXT
		// Just copy and paste
		
		outputFileName = outputDir.getAbsolutePath() + FILE_SEPERATOR  + "Context.java";
		outputFile = new File(outputFileName);
		if (!outputFile.exists()) {
			outputFile.createNewFile();
		}
		
		bufReader = new BufferedReader(new InputStreamReader(
				new FileInputStream(new File("ContextTemplate.txt"))));
		
		strBuf = new StringBuffer();
		
		// will leave this here just in case for templatizing down the line
		for (String tmp = null; (tmp = bufReader.readLine()) != null; tmp = null) {
			strBuf.append(tmp);
			strBuf.append(System.getProperty("line.separator"));
		}
		bufReader.close();

		PrintStream printStream3 = new PrintStream(new FileOutputStream(
				outputFileName));
		printStream3.println(strBuf.toString());
		printStream3.close();

		// ////////////////////////////////////////////////////////////////////////////////////////////////////
		// part 4 : parse TObject portions of ISL and build their respective
		// TOBJECT and OBject files.

		Set<String> keys = extractor.tObjectDefs.keySet();

		// Iterate over every TObject and generate their respective Object files
		for (String key : keys) {
			outputFileName = outputDir.getAbsolutePath() + FILE_SEPERATOR  + key + ".java";

			outputFile = new File(outputFileName);

			if (!outputFile.exists()) {
				outputFile.createNewFile();
			}

			// load TObject
			TObjectDefinition tObj = extractor.tObjectDefs.get(key);

			// Aggregate buffer by looping over each instance and generating
			buf.setLength(0);
			strBuf.setLength(0);

			// Generate the getters and setters for each.
			for (Instance instance : tObj.getInstances()) {
				String iType = instance.type;
				String iName = instance.name;
				String iOffset = Integer.toString(instance.offset);

				// Getter
				buf.append("\tpublic static ");
				buf.append(iType);
				buf.append(" get");
				buf.append(iName.substring(0, 1).toUpperCase()
						+ iName.substring(1));
				buf.append("(Object o) {\r\n\t\treturn MiscHeader.get");
				buf.append(iType.substring(0, 1).toUpperCase()
						+ iType.substring(1));
				buf.append(String.format("MetaData(o, %s);\r\n", iOffset));
				buf.append("\t}\r\n");

				// Setter
				buf.append("\tpublic static void set");
				buf.append(iName.substring(0, 1).toUpperCase()
						+ iName.substring(1));
				buf.append("(Object o, ");
				buf.append(iType);
				buf.append(" x) {\r\n\t\tMiscHeader.set");
				buf.append(iType.substring(0, 1).toUpperCase() + iType.substring(1));
				buf.append(String.format("MetaData(o, %s, x);\r\n", iOffset));
				buf.append("\t}\r\n");
			}

			bufReader = new BufferedReader(new InputStreamReader(
					new FileInputStream(new File("ObjectTemplate.txt"))));

			for (String tmp = null; (tmp = bufReader.readLine()) != null; tmp = null) {
				tmp = tmp.replaceAll("TEMP_CLASS", tObj.name);
				tmp = tmp.replaceAll("TEMP_HISTORY", className);
				tmp = tmp.replaceAll("TEMP_FUNCTION_DEFINITIONS",
						buf.toString());
				strBuf.append(tmp);
				strBuf.append(System.getProperty("line.separator"));
			}
			bufReader.close();

			// Write data to file
			printStream = new PrintStream(new FileOutputStream(outputFileName));
			printStream.println(strBuf.toString());
			printStream.close();
		}

		// Build the TObject.java file
		buf.setLength(0);
		strBuf.setLength(0);

		// Start generating the buffer
		if (keys.size() > 0) {
			// Wrap with static section
			buf.append("\tstatic {\n");

			// Inject the number of TObjects
			buf.append("\t\tTObjects = new TObject[");
			buf.append(keys.size() + "];\r\n");

			// Iterate over each key and append the new object
			int counter = 0;
			for (String key : keys) {
				// load TObject
				TObjectDefinition tObj = extractor.tObjectDefs.get(key);

				buf.append("\t\tTObjects[");
				buf.append(counter);
				buf.append("] = new ");
				buf.append(tObj.name);
				buf.append("(\"");
				buf.append(tObj.name);
				buf.append("\", ");
				buf.append(printcxtDefs.get(tObj.include));
				buf.append(", ALL, new ");
				buf.append(className);
				buf.append("());\r\n");

				counter++;
			}

			// Finish wrap
			buf.append("\t}");
		}

		bufReader = new BufferedReader(new InputStreamReader(
				new FileInputStream(new File("TObjectTemplate.txt"))));

		for (String tmp = null; (tmp = bufReader.readLine()) != null; tmp = null) {
			tmp = tmp.replaceAll("TEMP_STATIC", buf.toString());
			strBuf.append(tmp);
			strBuf.append(System.getProperty("line.separator"));
		}
		bufReader.close();

		outputFileName = outputDir.getAbsolutePath() + FILE_SEPERATOR + "TObject.java";
		
		outputFile = new File(outputFileName);

		if (!outputFile.exists()) {
			outputFile.createNewFile();
		}

		// Write data to file
		printStream = new PrintStream(new FileOutputStream(outputFileName));
		printStream.println(strBuf.toString());
		printStream.close();
	}
}
