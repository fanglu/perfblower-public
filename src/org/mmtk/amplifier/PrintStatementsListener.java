package org.mmtk.amplifier;
import org.antlr.v4.runtime.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PrintStatementsListener extends sdlParserBaseListener {
	sdlParserParser parser;

	public PrintStatementsListener(sdlParserParser parser) {
		this.parser = parser;
	}

	//Map<String, String> instNames = new HashMap<String, String>(); NOT SUPPORTED FOR MULTIPLE TOBJS
	Map<String, String> evStmts = new HashMap<String, String>();
	//Map<String, String> historyDefs = new HashMap<String, String>();
	String historyName = null;
	String historyDef =null;
	Map<String, String> partitionDefs = new HashMap<String, String>();
	Map<String, TObjectDefinition> tObjectDefs = new HashMap<String, TObjectDefinition>();
	
	String rtCtx = null;

//	public void exitInstDef(sdlParserParser.InstDefContext ctx) {
//		String instType = ctx.primitiveType().getText();
//		String instName = ctx.Identifier().getText();
//		instNames.put(instName, instType);
//	}

	public void exitE(sdlParserParser.EContext ctx) {
		TokenStream tokens = parser.getTokenStream();
		String evDef = null;
		String evStmt = null;
		if (ctx.ek() != null)
			evDef = tokens.getText(ctx.ek());
		if (ctx.eb() != null)
			evStmt = tokens.getText(ctx.eb());
		evStmts.put(evDef, evStmt);
	}

	public void exitH(sdlParserParser.HContext ctx) {
		TokenStream tokens = parser.getTokenStream();		
		if (ctx.Identifier() != null)
			historyName = ctx.Identifier().getText();
		if (ctx.hb() != null)
			historyDef = ctx.hb().STR().getText() + "|"
					+ tokens.getText(ctx.hb().expression());	
	}

	public void exitPb(sdlParserParser.PbContext ctx) {
		TokenStream tokens = parser.getTokenStream();
		String kind = null;
		String pbHistoryName = null;
		if (ctx.HISTORY2() != null)
			pbHistoryName = ctx.Identifier().getText();
		if (ctx.KIND() != null)
			kind = tokens.getText(ctx.pk());
		partitionDefs.put(pbHistoryName, kind);
	}
	
	// Creates a TObjectDefinition for every TObject in ISL
	public void exitO(sdlParserParser.OContext ctx) {
		TokenStream tokens = parser.getTokenStream();
		TObjectDefinition tObj = new TObjectDefinition();
		
		// Collect the name of the TObject
		tObj.name = ctx.Identifier().getText();
		
		// Loop over every tb in the context then test what type of db they are
		sdlParserParser.TbContext next = ctx.tb();
		while (next != null) {
			if (next.INCLUDE() != null) {
				tObj.include = next.Identifier().getText();
			}
			else if (next.PARTITION2() != null){
				tObj.partition = next.Identifier().getText();
			} else if (next.fd() != null) {
				if (next.fd().instDef() != null) {
					List<sdlParserParser.InstDefContext> fd = next.fd().instDef();
					for (int i = 0; i < fd.size(); i++) {
						Instance instance = new Instance();
						sdlParserParser.InstDefContext c = fd.get(i);
						
						// Initialize the instance
						instance.name = c.Identifier().getText();
						instance.type = c.primitiveType().getText();
						
						// Add instance to the List of instances in TObj
						try {
							if (tObj.AddInstance(instance) == -1) {
								System.out.println("Instances cannot total greater than 32 bits");
								System.exit(0);
							} 
						} catch (Exception e) {
							System.out.println(e.getMessage());
							System.exit(0);
						}
					}
					
				}
			}
			next = next.tb();
		} 
		
		tObjectDefs.put(tObj.name, tObj);
	}

}
