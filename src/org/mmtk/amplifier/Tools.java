package org.mmtk.amplifier;
import java.util.ArrayList;

public final class Tools {

	// make the first letter of the string to upper case
	public static String firstLetterToUpper(String str) {
		char[] array = str.toCharArray();
		if (Character.isLowerCase(array[0]))
			array[0] -= 32;
		return String.valueOf(array);
	}

	// get the offset value according to the data type
	public static int getTypeValue(String input) {
		int ret = 0;
		if (input.equals("long") == true)
			ret = 64;
		if (input.equals("double") == true)
			ret = 64;
		if (input.equals("int") == true)
			ret = 32;
		if (input.equals("float") == true)
			ret = 32;
		if (input.equals("short") == true)
			ret = 16;
		if (input.equals("char") == true)
			ret = 16;
		if (input.equals("byte") == true)
			ret = 8;
		if (input.equals("boolean") == true)
			ret = 1;

		return ret;

	}

	// get the ordering value according to the data type
	public static int getOrderValue(String input) {
		int ret = -1;
		if (input.equals("long") == true)
			ret = 0;
		if (input.equals("double") == true)
			ret = 1;
		if (input.equals("int") == true)
			ret = 2;
		if (input.equals("float") == true)
			ret = 3;
		if (input.equals("short") == true)
			ret = 4;
		if (input.equals("char") == true)
			ret = 5;
		if (input.equals("byte") == true)
			ret = 6;
		if (input.equals("boolean") == true)
			ret = 7;

		return ret;

	}

	public static String getRetValue(String input) {
		String ret = null;
		if (input.equals("long") == true)
			ret = "0";
		if (input.equals("double") == true)
			ret = "0";
		if (input.equals("int") == true)
			ret = "0";
		if (input.equals("float") == true)
			ret = "0";
		if (input.equals("short") == true)
			ret = "0";
		if (input.equals("char") == true)
			ret = "0";
		if (input.equals("byte") == true)
			ret = "0";
		if (input.equals("boolean") == true)
			ret = "false";

		return ret;
	}

	public static Boolean allFlagSetted(ArrayList<ctxNext> cList) {
		for (int i = 0; i < cList.size(); i++) {
			if (cList.get(i).getFlag() == 0)
				return false;
		}
		return true;

	}
}
